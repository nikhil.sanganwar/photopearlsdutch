//
//  AppDelegate.h
//  PhotoPearls
//
//  Created by Priyesh Das on 10/28/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSDictionary *userDetails;

@property (strong, nonatomic) NSIndexPath *selectedMenu;

+ (AppDelegate *)delegate;

@end

