//
//  UIView+Addition.h
//  Lo Quiero
//
//  Created by Priyesh Das on 8/6/15.
//  Copyright (c) 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Addition)

- (void)makeRound;

-(void)setCornerRadius:(float)radius;

-(void)setBorderWithColor:(UIColor *)color withBorderWidth:(float)width;

-(void)setViewIsEmptyText;

-(void)setShadowWithColor:(UIColor *)color withOpacity:(float)opacity withRadius:(float)radius andOffset:(float)offset ;
@end
