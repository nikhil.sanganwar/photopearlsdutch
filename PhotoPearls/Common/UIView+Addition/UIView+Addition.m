//
//  UIView+Addition.m
//  Lo Quiero
//
//  Created by Priyesh Das on 8/6/15.
//  Copyright (c) 2015 Teknowledge Software. All rights reserved.
//

#import "UIView+Addition.h"

@implementation UIView (Addition)

- (void)makeRound
{
    [self.layer setCornerRadius:self.frame.size.height / 2.0];
    [self.layer setMasksToBounds:YES];
}

-(void)setCornerRadius:(float)radius
{
    [self.layer setCornerRadius:radius];
    [self.layer setMasksToBounds:YES];
}

-(void)setBorderWithColor:(UIColor *)color withBorderWidth:(float)width
{
    [self.layer setBorderColor:color.CGColor];
    [self.layer setBorderWidth:width];
}

-(void)setViewIsEmptyText {
    
    UILabel *lblEMptyText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    lblEMptyText.text = @"Sorry, there seems to be no data here";
    lblEMptyText.textColor = [UIColor lightGrayColor];
    lblEMptyText.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lblEMptyText];
    [lblEMptyText setCenter:self.center];
}

-(void)setShadowWithColor:(UIColor *)color withOpacity:(float)opacity withRadius:(float)radius andOffset:(float)offset {
    self.layer.allowsEdgeAntialiasing = YES;
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [color CGColor];
    self.layer.shadowOffset = CGSizeMake(1.0, offset);
    self.layer.shadowOpacity = opacity;
    self.layer.shadowRadius = radius;
    
}
@end
