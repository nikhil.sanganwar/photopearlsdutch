//
//  UtilityView.h
//  PhotoPearls
//
//  Created by C-H on 2010-10-26.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PHPSession.h"

@interface UtilityView : UIView

@property(nonatomic, assign) CGRect myArea;
@property(nonatomic, assign) CGRect cropWindow;
@property(nonatomic, retain) UIResponder* responderBelow;
@property(nonatomic) int x;
@property(nonatomic) int y;
@property(nonatomic, assign) bool responsive;

@end
