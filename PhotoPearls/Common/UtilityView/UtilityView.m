//
//  UtilityView.m
//  PhotoPearls
//
//  Created by C-H on 2010-10-26.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "UtilityView.h"


@implementation UtilityView

@synthesize myArea, cropWindow, responderBelow, responsive, x,y;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
	responsive = NO;
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGRect leftRect = CGRectMake(0, 0, cropWindow.origin.x , myArea.size.height);
	CGRect rightRect = CGRectMake(cropWindow.origin.x + cropWindow.size.width,
								  0,
								  myArea.size.width - (cropWindow.origin.x + cropWindow.size.width),
								  myArea.size.height);
	CGRect topRect = CGRectMake(cropWindow.origin.x + 0.5,
								0,
								cropWindow.size.width - 1.0,
								cropWindow.origin.y);
	CGRect bottomRect = CGRectMake(cropWindow.origin.x + 0.5,
								   cropWindow.origin.y + cropWindow.size.height,
								   cropWindow.size.width - 1.0, 
								   myArea.size.height - (cropWindow.origin.y + cropWindow.size.height));
	UIColor* fillColor = [[UIColor alloc] initWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
	CGContextSetFillColorWithColor(context, [fillColor CGColor]);
	CGContextAddRect(context, leftRect);
	CGContextAddRect(context, rightRect);
	CGContextAddRect(context, topRect);
	CGContextAddRect(context, bottomRect);
	CGContextDrawPath(context, kCGPathFillStroke);
	//CGContextAddRect(context, cropWindow);
	CGContextSetStrokeColorWithColor(context, [[UIColor whiteColor] CGColor]);
	//CGContextStrokePath(context);
	
	CGFloat dist = cropWindow.size.width / x;
	CGFloat position = cropWindow.origin.x;
	for (int i = 0; i < x; i++) {
		CGContextMoveToPoint(context, position, cropWindow.origin.y);
		CGContextAddLineToPoint(context, position, cropWindow.origin.y+cropWindow.size.height);
		CGContextStrokePath(context);
		position+=dist;
	}
	dist = cropWindow.size.height / y;
	position = cropWindow.origin.y;
	for (int i = 0; i < y; i++) {
		CGContextMoveToPoint(context, cropWindow.origin.x, position);
		CGContextAddLineToPoint(context, cropWindow.origin.x+cropWindow.size.width, position);
		CGContextStrokePath(context);
		position+=dist;
	}
}


//- (void)dealloc {
//    [super dealloc];
//}


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
	if (responsive) {
		return YES;
	} else {
		return NO;
	}
}

@end
