//
//  WebshopViewCell.h
//  PhotoPearls
//
//  Created by Priyesh Das on 11/4/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebshopViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblWebShopName;
@property (weak, nonatomic) IBOutlet UIButton *btnGoToShop;

@end
