//
//  ShoppingListCell.m
//  PhotoPearls
//
//  Created by Priyesh Das on 11/4/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "ShoppingListCell.h"

@implementation ShoppingListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
