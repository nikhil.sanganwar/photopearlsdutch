//
//  ShoppingListCell.h
//  PhotoPearls
//
//  Created by Priyesh Das on 11/4/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgBeadType;
@property (weak, nonatomic) IBOutlet UILabel *lblBeadCount;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (nonatomic, retain) UISegmentedControl* myControl;
@end
