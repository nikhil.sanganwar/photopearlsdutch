//
//  CustomLabel.m
//  Cramway
//
//  Created by Mahesh Agrawal on 4/24/15.
//  Copyright (c) 2015 Teknowledge Software. All rights reserved.
//

#import "CustomLabel.h"
#import "Common.h"

@implementation CustomLabel

- (void)layoutSubviews
{
    [super layoutSubviews];
    if ([Common isiPad]) {
        //self.font.fontName
        self.font = [UIFont fontWithName:@"Helvetica-Bold" size:22];
    }else {
        self.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    }
//    int sizeOfFont = ((self.frame.size.height / self.numberOfLines) - 4);
//    self.font = [UIFont fontWithName:self.font.fontName size:sizeOfFont];
//    [self layoutIfNeeded];
//    self.adjustsFontSizeToFitWidth = YES;
}

-(void)autoIncreaseFont
{
    self.font = [UIFont fontWithName:self.font.fontName size:((self.frame.size.height / self.numberOfLines) - 4)];
}

-(void)setFontSize:(float)size
{
    self.font = [UIFont fontWithName:self.font.fontName size:size];
}

@end
