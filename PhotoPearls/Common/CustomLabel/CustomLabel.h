//
//  CustomLabel.h
//  Cramway
//
//  Created by Mahesh Agrawal on 4/24/15.
//  Copyright (c) 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLabel : UILabel

-(void)autoIncreaseFont;

-(void)setFontSize:(float)size;

@end
