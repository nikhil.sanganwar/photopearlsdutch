//
//  ButtonView.m
//  PhotoPearls
//
//  Created by Priyesh Das on 11/2/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "ButtonView.h"
#import "Common.h"

@interface ButtonView ()

@property (nonatomic) CGRect origFrame;
@property int originX;
@property int originY;

@end

@implementation ButtonView

-(void)layoutSubviews {
    if (IPAD && self.tag != 100) {
        CGRect frame = self.frame;
        frame.size.height = frame.size.height/1.1;
        frame.size.width = frame.size.width/1.2;
        self.frame = frame;
        self.translatesAutoresizingMaskIntoConstraints = YES;
    }
}

-(void)setupEmboss {
    if (self.tag == 100) {
        _imgEmbossView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"rounded_square_518x330"]];
        _origFrame = self.frame;
        _originX = self.frame.origin.x;
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(handleSingleTap:)];
        [self addGestureRecognizer:singleFingerTap];
    }
//    else {
//       _imgEmbossView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"knapp_1"]];
//    }
//    [self addSubview:_imgEmbossView];
//    [self sendSubviewToBack:_imgEmbossView];
//    [self setBackgroundColor:[UIColor clearColor]];
//    [self addFullResizeConstraints:_imgEmbossView addedOnParent:self];
}
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    UIButton *btn = [[UIButton alloc]initWithFrame:self.superview.frame];
    //CGRect frame = self.superview.frame;
//    if (frame.origin.x == self.superview.frame.origin.x) {
//        frame = self.frame;
//        frame = _origFrame;
//        frame.origin.x = _originX;
//        self.frame= frame;
//    }
//    else {
//        frame.origin.x = self.superview.frame.origin.x;
//        frame.size.width = self.superview.frame.size.width;
//        frame.size.height = self.superview.frame.size.width;
//        self.frame = frame;
//        
//    }
//    self.translatesAutoresizingMaskIntoConstraints = YES;
//    btn.frame = frame;
    [btn setBackgroundColor:[UIColor whiteColor]];
//    btn.imageView.contentMode = UIViewContentModeScaleToFill;
    for (UIView *subview in [self subviews])
    {
        NSLog(@"%@", subview);
        // ---------- remember one thing there should be one imageview ------
        if([subview isKindOfClass:[UIImageView class]])
        {
            UIImageView *imgVw = (UIImageView *)subview;
            
            //here the image size of the given image is increased so that the image enlarges a bit but still maintains the ratio so that it doesn't get distorted
            CGFloat aspectRatio;
            CGRect imgFrame;
            int imgWidth = imgVw.image.size.width;
            int imgHeight = imgVw.image.size.height;
            
            //calculate if the aspect ratio based on the height and width
            if (imgHeight > imgWidth){
                //if the height of the image is greater than the width then calculate the difference in width of the larger image from the shorter one and accordingly calculate the larger height
//                aspectRatio = (CGFloat)imgWidth/imgHeight;
                
                //hence calculating aspectRatio in accordance to the different widths
                CGFloat viewWidth = self.superview.frame.size.width - 20;
                aspectRatio = viewWidth/imgWidth;
                CGFloat extHeight = imgHeight * aspectRatio;
                
                imgFrame = CGRectMake(0, 0, (self.superview.frame.size.width - 20),extHeight);
            }
            else {
                aspectRatio = (CGFloat)imgHeight/imgWidth;
                imgFrame = CGRectMake(0, 0, self.superview.frame.size.width - 20, (self.superview.frame.size.width - 20) * aspectRatio);
            }
            
            
            UIImage *img = [self image:imgVw.image scaledToSize:imgFrame.size];
           [btn setImage:img forState:UIControlStateNormal];
        }
    }
    [btn addTarget:self action:@selector(hideButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.superview addSubview:btn];
    
}

//scale the image to the given size
- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }
    
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return image;
}

-(IBAction)hideButton :(UIButton *)sender {
    
    sender.hidden = YES;
    [sender removeFromSuperview];
}

-(void)revertEmbossImage {
    UIImage* flippedImage = [UIImage imageNamed:@"knapp_2"];
    [UIView animateWithDuration:0.2 animations:^{
         _imgEmbossView = [[UIImageView alloc]initWithImage:flippedImage];
    } completion:^(BOOL finished) {
         _imgEmbossView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"knapp_1"]];
    }];
}


//adds leading, trailing, top and bottom constraint to the parent view from the sub view

-(void)addFullResizeConstraints:(UIView *)subView addedOnParent:(UIView *)parent{
    
    
    
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   
                                   constraintWithItem:subView
                                   
                                   attribute:NSLayoutAttributeTrailing
                                   
                                   relatedBy:NSLayoutRelationEqual
                                   
                                   toItem:parent
                                   
                                   attribute:NSLayoutAttributeTrailing
                                   
                                   multiplier:1.0
                                   
                                   constant:0.f];
    
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 
                                 constraintWithItem:subView
                                 
                                 attribute:NSLayoutAttributeBottom
                                 
                                 relatedBy:NSLayoutRelationEqual
                                 
                                 toItem:parent
                                 
                                 attribute:NSLayoutAttributeBottom
                                 
                                 multiplier:1.0
                                 
                                 constant:0.f];
    
    NSLayoutConstraint *top = [NSLayoutConstraint
                               
                               constraintWithItem:subView
                               
                               attribute:NSLayoutAttributeTop
                               
                               relatedBy:NSLayoutRelationEqual
                               
                               toItem:parent
                               
                               attribute:NSLayoutAttributeTop
                               
                               multiplier:1.0
                               
                               constant:0.f];
    
    
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   
                                   constraintWithItem:subView
                                   
                                   attribute:NSLayoutAttributeLeading
                                   
                                   relatedBy:NSLayoutRelationEqual
                                   
                                   toItem:parent
                                   
                                   attribute:NSLayoutAttributeLeading
                                   
                                   multiplier:1.0
                                   
                                   constant:0.f];
    
    [parent addConstraint:trailing];
    
    [parent addConstraint:bottom];
    
    [parent addConstraint:top];
    
    [parent addConstraint:leading];
    
}

@end
