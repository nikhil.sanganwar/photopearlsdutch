//
//  ButtonView.h
//  PhotoPearls
//
//  Created by Priyesh Das on 11/2/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ButtonView : UIView

-(void)setupEmboss;
@property (strong,nonatomic)UIImageView *imgEmbossView;
-(void)revertEmbossImage ;
@end
