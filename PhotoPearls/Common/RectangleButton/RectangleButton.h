//
//  RectangleButton.h
//  TeamCommish
//
//  Created by Priyesh Das on 10/14/15.
//  Copyright © 2015 Teknowledge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RectangleButton : UIButton
-(void)setHighlightAndShadow ;
@end
