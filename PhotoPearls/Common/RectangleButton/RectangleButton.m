//
//  RectangleButton.m
//  TeamCommish
//
//  Created by Priyesh Das on 10/14/15.
//  Copyright © 2015 Teknowledge. All rights reserved.
//

#import "RectangleButton.h"
#import "Common.h"

@implementation RectangleButton

-(void)layoutSubviews {
    [super layoutSubviews];
    
    if ([Common isiPad]) {
        self.titleLabel.font = [UIFont fontWithName:@"Arnold21" size:32];
    }
    else {
        self.titleLabel.font = [UIFont fontWithName:@"Arnold21" size:28];
    }
        
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self setBackgroundImage:[UIImage imageNamed:@"knapp_1"] forState:UIControlStateNormal];
//    [self setBackgroundImage:[UIImage imageNamed:@"knapp_2"] forState:UIControlStateHighlighted];
    [self setHighlightAndShadow];
//    [self drawTextInRect:self.titleLabel.layer.frame];
    //[self addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
}

//-(void)onClick:(UIButton *)btn {
//    UIImage* flippedImage = [UIImage imageNamed:@"knapp_2"];
//    [UIView animateWithDuration:0.2 animations:^{
//        [self setBackgroundImage:flippedImage forState:UIControlStateNormal];
//    } completion:^(BOOL finished) {
//        [self setBackgroundImage:[UIImage imageNamed:@"knapp_1"] forState:UIControlStateNormal];
//    }];
//
//    
//}
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    if (self.state == UIControlStateHighlighted) {
//        [self setHighlighted:NO];
//    }
//}


- (void)drawTextInRect:(CGRect)rect {
    
    NSLog(@"button text %@",self.titleLabel.text);
    CGSize shadowOffset = self.titleLabel.layer.shadowOffset;
    UIColor *textColor = self.titleLabel.textColor;
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(c, 1);
    CGContextSetLineJoin(c, kCGLineJoinRound);
    
    CGContextSetTextDrawingMode(c, kCGTextStroke);
    self.titleLabel.textColor = [UIColor whiteColor];
    [super.titleLabel drawTextInRect:rect];
    
    CGContextSetTextDrawingMode(c, kCGTextFill);
    self.titleLabel.textColor = textColor;
    self.titleLabel.layer.shadowOffset = CGSizeMake(0, 0);
    [super.titleLabel drawTextInRect:rect];
    
    self.titleLabel.layer.shadowOffset = shadowOffset;
    
}
-(void)setHighlightAndShadow
{
    self.titleLabel.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.titleLabel.layer.masksToBounds = NO;
    self.titleLabel.layer.shadowOpacity = 0.8;
    self.titleLabel.layer.shadowRadius = 0.2;
    self.titleLabel.layer.shadowOffset = CGSizeMake(3.0f,3.0f);
//    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
//    [self addTarget:self action:@selector(changeBackgroundColor:) forControlEvents:UIControlEventTouchDown];
}

//- (void)drawTextInRect:(CGRect)rect {
//    
//    CGSize shadowOffset = self.shadowOffset;
//    UIColor *textColor = self.textColor;
//    
//    CGContextRef c = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(c, 1);
//    CGContextSetLineJoin(c, kCGLineJoinRound);
//    
//    CGContextSetTextDrawingMode(c, kCGTextStroke);
//    self.textColor = [UIColor whiteColor];
//    [super drawTextInRect:rect];
//    
//    CGContextSetTextDrawingMode(c, kCGTextFill);
//    self.textColor = textColor;
//    self.shadowOffset = CGSizeMake(0, 0);
//    [super drawTextInRect:rect];
//    
//    self.shadowOffset = shadowOffset;
//    
//}

-(IBAction)changeBackgroundColor:(UIButton *)sender {
    
    UIColor *btnDefColor = sender.backgroundColor;
    [UIView animateWithDuration:0.5 animations:^{
        [sender setBackgroundColor:themeLightBlueColor()];
    } completion:^(BOOL finished) {
        [sender setBackgroundColor:btnDefColor];
    }];
}
@end
