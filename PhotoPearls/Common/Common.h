//
//  Common.h
//  TeamCommish
//
//  Created by Zeeshan Ahmed on 28/09/15.
//  Copyright © 2015 Teknowledge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface Common : NSObject

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone
#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif

#ifdef APIDEBUG
#   define APILog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define APILog(...)
#endif

#define FB_APP_ID @"1703876399840302"

//declaring enums for the selection of which type of api request is made,this enum is mostly used in apimanager class with different classes already selecting their type and sending request to apimanager
typedef enum
{
    getPearlifiedImage = 1,
    getShopsConnection,
    getWebshops,
    sendPattern,
    defaultImageReq,
    getStore,
    getInspiration
} getUrlType;


extern UIColor *themeBlueColor();
extern UIColor *themeLightGreyColor();
extern UIColor *themeGreenColor () ;
extern UIColor *themePurpleColor () ;
extern UIColor *themeLightBlueColor () ;
extern UIColor *themeButtonBlueColor () ;
extern UIColor *themeLightPurpleColor () ;

extern BOOL isEmpty (id value);
extern BOOL isEmailValid (NSString * email);
+(BOOL)isiPad;
+(NSDictionary *)loadCustomObjectWithKey:(NSString *)key ;
+(void)saveCustomObject:(NSDictionary *)object key:(NSString *)key ;
+ (void)setUserDefaults:(NSString *)key value:(NSString *)value;
+ (void)deleteUserDefaults:(NSString *)key;
+ (NSString *)getUserDefaults:(NSString *)key;
+ (NSData *)getImageDataFromDefaults:(NSString *)key;


extern NSString *getImageString (UIImage *profileImage);
extern NSData *getImageData (UIImage *profileImage) ;
extern UIImage *getImageFromData (NSData *dataImage) ;
+(void)setImageDataToDefaults:(NSString *)key value:(NSData *)value;
extern UIImage *getImageFromString (NSString *imageString);
#pragma mark - View Size -

extern float originX(UIView *view);
extern float originY(UIView *view);
extern float height(UIView *view);
extern float width(UIView *view);


extern void showHud(UIView * view);
extern void hideHud(UIView *view);
+(MBProgressHUD *)showCustomHud:(NSString *)label label2:(NSString *)label2 ;
extern bool checkInternetConnection();
+(void)showToastMessage:(NSString *)message withTitle:(NSString *)title withAction:(BOOL)action withMessageType:(BOOL)messageType;


extern NSString *keyUserDetails;
extern NSString *keyUserFirstName;
extern NSString *keyUserLastName;
extern NSString *keyOfflineMessage;
extern NSString *keyxBoards;
extern NSString *keyyBoards;
extern NSString *keyUserImage;
extern NSString *keyScaledImage;
extern NSString *keyPearledImage;
extern NSString *keyAPIGoogle;

@end
