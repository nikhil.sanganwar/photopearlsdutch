//
//  Common.m
//  TeamCommish
//
//  Created by Zeeshan Ahmed on 28/09/15.
//  Copyright © 2015 Teknowledge. All rights reserved.
//

#import "Common.h"
#import "AppDelegate.h"
#import <CRToast/CRToast.h>
#import "Reachability.h"
#import <SpinKit/RTSpinKitView.h>
#import "LocalizationSystem.h"

@implementation Common

UIColor *themeBlueColor () {
    return [UIColor colorWithRed:20.0/255.0 green:130.0/255.0 blue:214.0/255.0 alpha:1.0];
}
UIColor *themeButtonBlueColor () {
    return [UIColor colorWithRed:21.0/255.0 green:112.0/255.0 blue:195.0/255.0 alpha:1.0];
}
UIColor *themeLightBlueColor () {
    return [UIColor colorWithRed:(0.0/255.0) green:(173/255.0) blue:(239/255.0) alpha:1.0];
}
UIColor *themeGreenColor () {
    return [UIColor colorWithRed:39.0/255.0 green:195.0/255.0 blue:43.0/255.0 alpha:0.5];
}
UIColor *themeLightPurpleColor () {
    return [UIColor colorWithRed:78.0/255.0 green:70.0/255.0 blue:164.0/255.0 alpha:1.0];
}
UIColor *themePurpleColor () {
    return [UIColor colorWithRed:94.0/255.0 green:93.0/255.0 blue:200.0/255.0 alpha:1.0];
}
UIColor *themeLightGreyColor () {
    return [UIColor colorWithRed:226.0/255.0 green:226.0/255.0 blue:226.0/255.0 alpha:1.0];
}

+(BOOL)isiPad{
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
}
BOOL isEmpty (id value) {
    
    return (value == (id)[NSNull null] || value == nil || ([value isKindOfClass:[NSString class]] && ([value isEqualToString:@""] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"(null)"]))) ? YES : NO;
}

BOOL isEmailValid(NSString * email) {
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+(void)setUserDefaults:(NSString *)key value:(NSString *)value{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)setImageDataToDefaults:(NSString *)key value:(NSData *)value{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(void)deleteUserDefaults:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSData *)getImageDataFromDefaults:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+ (NSString *)getUserDefaults:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}


+(void)saveCustomObject:(NSDictionary *)object key:(NSString *)key {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:object forKey:key];
    [defaults synchronize];
    
}


+(NSDictionary *)loadCustomObjectWithKey:(NSString *)key {
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    NSData *encodedObject = [defaults objectForKey:key];
    //    User *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    
    NSDictionary *dictUserDetails = [[NSMutableDictionary alloc]init];
    dictUserDetails = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    return dictUserDetails;
}

#pragma mark - View Size -

float originX(UIView *view) {
    return view.frame.origin.x;
}

float originY(UIView *view) {
    return view.frame.origin.y;
}

float height(UIView *view) {
    return view.frame.size.height;
}

float width(UIView *view) {
    return view.frame.size.width;
}

NSString *getImageString (UIImage *profileImage) {
    // From image to data
    NSData *dataImage = [NSData dataWithData:UIImagePNGRepresentation(profileImage)];
    return [dataImage base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

NSData *getImageData (UIImage *profileImage) {
    // From image to data
    NSData *dataImage = [NSData dataWithData:UIImagePNGRepresentation(profileImage)];
    return dataImage;
}
UIImage *getImageFromData (NSData *dataImage) {
    return [UIImage imageWithData:dataImage];
}

UIImage *getImageFromString (NSString *imageString) {
    NSData *dataImage = [[NSData alloc] initWithBase64EncodedString:imageString options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:dataImage];
}

//-------------------------------------------------------------------------

#pragma mark - Show/Hide Progress HUD -

//-------------------------------------------------------------------------

void showHud(UIView * view) {
    //[AppDelegate delegate].window.userInteractionEnabled=NO;
    hideHud(view);
    view.userInteractionEnabled=NO;
    [MBProgressHUD showHUDAddedTo:view animated:YES];
}
void hideHud(UIView *view) {
    //[AppDelegate delegate].window.userInteractionEnabled=YES;
    view.userInteractionEnabled=YES;
    [MBProgressHUD hideHUDForView:view animated:YES];
}

//the code is modified as client wants only the normal activity indicator and this method was implemented
+(MBProgressHUD *)showCustomHud:(NSString *)label label2:(NSString *)label2 {
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = CGRectGetWidth(screenBounds);
    RTSpinKitView *spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleChasingDots];
    spinner.center = CGPointMake(CGRectGetMidX(screenBounds), CGRectGetMidY(screenBounds));
    spinner.sizeToFit;
    [spinner startAnimating];
    MBProgressHUD *hud = [[MBProgressHUD alloc]init];//showHUDAddedTo:self.view animated:YES];
   // hud.square = YES;
    hud.color = [UIColor clearColor];
    hud.mode = MBProgressHUDModeIndeterminate;
//    hud.customView = spinner;
//    hud.labelText = [NSString stringWithFormat:@"%@",AMLocalizedString(label, nil)];
//    hud.detailsLabelText = [NSString stringWithFormat:@"%@",AMLocalizedString(label2, nil)];
    return hud;
}


#pragma mark  Check Internet Connection
bool checkInternetConnection()
{
    Reachability *networkReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    //    if(!networkStatus)
    //    {
    //        [self showAlert:@"" andMsg:@"No internet connection detected!"];
    //
    //    }
    return networkStatus;
    
}

//this method is modified to show uialertview, as this method is being used everywhere hence only updating this method
+(void)showToastMessage:(NSString *)message withTitle:(NSString *)title withAction:(BOOL)action withMessageType:(BOOL)messageType
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    
//    NSMutableDictionary *options = [@{kCRToastAnimationInDirectionKey        :  @(CRToastAnimationDirectionLeft),
//                                      kCRToastAnimationInTypeKey             :  @(CRToastAnimationTypeSpring),
//                                      kCRToastAnimationOutDirectionKey       :  @(CRToastAnimationDirectionRight),
//                                      kCRToastAnimationOutTypeKey            :  @(CRToastAnimationTypeSpring),
//                                      kCRToastTextAlignmentKey               :  @(NSTextAlignmentLeft),
//                                      kCRToastBackgroundColorKey            : [UIColor yellowColor],
//                                      kCRToastTextKey                        :  [NSString stringWithFormat:@"%@",AMLocalizedString(title, nil)],
//                                      kCRToastTimeIntervalKey                :  @(1.5),
//                                      kCRToastSubtitleTextKey                :  [NSString stringWithFormat:@"%@",AMLocalizedString(message, nil)],
//                                      kCRToastSubtitleTextAlignmentKey       :  @(NSTextAlignmentLeft),
//                                      kCRToastNotificationTypeKey            :  @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey:  @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey              :  @(YES),
//                                      kCRToastFontKey                        :   [UIFont fontWithName:@"HelveticaNeue-Bold" size:16],
//                                      } mutableCopy];
//    
//    if (messageType) {
//        options[kCRToastTextColorKey] = themeLightGreyColor();
//        options[kCRToastSubtitleTextColorKey] = themeLightGreyColor();
//    }else{
//        options[kCRToastTextColorKey] = [UIColor redColor];
//        options[kCRToastSubtitleTextColorKey] = [UIColor redColor];
//    }
//    
//    if (action) {
//        options[kCRToastInteractionRespondersKey] = @[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeTap
//                                                                                                      automaticallyDismiss:YES
//                                                                                                                     block:^(CRToastInteractionType interactionType){
//                                                                                                                     }]];
//    }
//    
//    [CRToastManager showNotificationWithOptions:options completionBlock:nil];
    
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"OK Tapped. Hello World!");
    }
}



NSString *keyUserDetails        =   @"user_details";
NSString *keyUserFirstName      =   @"first_name";
NSString *keyUserLastName       =   @"last_name";
NSString *keyOfflineMessage     =   @"Unable to reach server.Check your internet connection or try later";
NSString *keyxBoards            =   @"xBoards";
NSString *keyyBoards            =   @"yBoards";
NSString *keyUserImage          =   @"userImage";
NSString *keyScaledImage        =   @"scaledImage";
NSString *keyPearledImage       =   @"pearledImage";
NSString *keyAPIGoogle          =   @"AIzaSyDwXxdRLDXHWIOztxjlmi5YFOjb_SqBBXg";

@end
