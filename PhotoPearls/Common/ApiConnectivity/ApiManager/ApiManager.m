//
//  ApiManager.m
//  Fisho
//
//  Created by Priyesh Das on 3/19/15.
//  Copyright (c) 2015 teks. All rights reserved.
//

#import "ApiManager.h"
#import <AFNetworking.h>
#import "Common.h"
#import <CoreLocation/CoreLocation.h>

@interface ApiManager ()
@property (strong,nonatomic)NSURLConnection* imageConnection;
@property (strong,nonatomic)NSMutableData* recievedData;
@end

@implementation ApiManager
{
    NSURLConnection* settingsBundleConnection;
    NSURLConnection* pearlifiedImageConnection;
    NSURLConnection* pearlPatternConnection;
}
@synthesize imageConnection;
@synthesize recievedData;
@synthesize currentSelection;

//keys whch are used in the apimanager
NSString *baseURL                                   =   @".api.photopearls.se/v1.1/";
NSString *keyHTTPS                                  =   @"https://";
NSString *keyEchoHeaders                            =   @"EchoHeaders";
NSString *keyPartnerLogoByCountry                   =   @"GetPartnerLogoByCountry";
NSString *keyAddImage                               =   @"AddImage";
NSString *keysetSettingsBundle                      =   @"SetSettingsBundle";
NSString *keyGetPearlifiedImage                     =   @"GetPearlifiedImage";
NSString *keyGetPearlPattern                        =   @"GetPearlPattern";
NSString *keyGetInspirationByCountryAndLanguage     =   @"GetInspirationByCountryAndLanguage";
NSString *keyGetPartnerStoresByCountry              =   @"GetPartnerStoresByCountry";
NSString *GetPartnerWebshopsByCountry               =   @"GetPartnerWebshopsByCountry";
NSString *keySendPearlPatternPrintout               =   @"SendPearlPatternPrintout";

//keys to find store location
NSString *keyUserName           =   @"munkplast-iphone";
NSString *keyPassword           =   @"yu!sux!Q";
NSString *keyCountry            =   @"last_name";
NSString *keyLanguage           =   @"email";
NSString *keyImage_ID           =   @"hobbies";
NSString *keyRecipient          =   @"tagline";
NSString *keyLatitude           =   @"userImage";
NSString *keyLongitude          =   @"userScaledImage";
NSString *keyWidth              =   @"occupation";
NSString *keyHeight             =   @"user_details";
NSString *keyPattern            =   @"first_name";
NSString *keyUrl                =   @"last_name";
NSString *keyStores             =   @"email";
NSString *keyName               =   @"hobbies";
NSString *keyCountryCode        =   @"tagline";
NSString *keyStreetAddress      =   @"userImage";
NSString *keyDistnace           =   @"userScaledImage";
NSString *keyState              =   @"occupation";
NSString *keyCity               =   @"userScaledImage";
NSString *keyWebshops           =   @"occupation";

+(void)getBaseServer {
    int serverNo = arc4random()%26 + 97;
    NSString* urlString = [NSString stringWithFormat:@"https://%C.api.photopearls.se/v1.1/", (unichar)serverNo];
    NSLog(@"%@", urlString);
    baseURL = urlString;
}

+(void)requestApiCall:(NSDictionary *)params andUrl:(NSString *)urlPath withSuccess:(void (^) (AFHTTPRequestOperation *operation, id responseObject))success withFailure:(void (^) (AFHTTPRequestOperation *operation, NSError *error))failure {
     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *authStr = @"user1:pass";
    
    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];;
    
    //NSError** error = [[NSError alloc] init];
    // Add image to server
    NSString* urlString = [[NSString alloc] initWithFormat:@"%@/EchoHeaders", urlPath];
    NSMutableURLRequest *request =
    [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    //[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.photopearls.se/photopearls-server/AddImage"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: [params valueForKey:@"imageData"]];
    
    AFHTTPRequestOperation *opManager = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    opManager.responseSerializer = [AFJSONResponseSerializer serializer];
    [opManager setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON responseObject: %@ ",responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
        
    }];
    [opManager start];
    
}


# pragma mark
# pragma mark - Pearlification
+(void)sendImageForPearlification:(NSData *)imageData withSuccess:(void (^) (id result))success withFailure:(void (^) (NSError *error))failure {
    //email, username, password, contact, profileimage, timelineimage
    [self getBaseServer];
    NSString *urlString= [NSString stringWithFormat:@"%@%@",baseURL,keyAddImage];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSDictionary *parameters = @{@"imageData"            : imageData
                                
                                 };
    
    [self requestApiCall:parameters andUrl:urlString withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // NSString* strResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        APILog(@"\n\n\nresponse - %@",responseObject);
        //id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        success(responseObject);
    } withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
        APILog(@"\n\n\nresponse -  = %@",operation.responseString);
        APILog(@"\n\n\nresponse description - %@",error.description);
        failure(error);
    }];
    
}

-(void)initWithRequest:(NSMutableURLRequest *)request fromDelegate:(BaseViewController *)delegate {
    self.baseDelegate = delegate;
    imageConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse*)response {
    recievedData = [[NSMutableData alloc] init];
    if (currentSelection == sendPattern) {
        [_baseDelegate receivedData:response anderrorMsg:nil];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [recievedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    //    SBJsonParser* parser = [[SBJsonParser alloc] init];
    //    SBJsonWriter* writer = [[SBJsonWriter alloc] init];
    
    
    if (currentSelection == getPearlifiedImage) {
        NSString* urlString;
        NSMutableURLRequest* request;
        NSError *jsonError;
        
        if (connection == imageConnection) {
            NSString *returnString = [[NSString alloc] initWithData:recievedData encoding: NSUTF8StringEncoding];
            NSData *objectData = [returnString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary* returnDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                       options:NSJSONReadingMutableContainers
                                                                         error:&jsonError];
            self.session.imageId = (NSString*)[returnDict objectForKey:@"imageId"];
            imageConnection = nil;
            
            NSMutableDictionary* settingsDict = [NSMutableDictionary dictionary];
            self.session.xBoards = [[Common getUserDefaults:keyxBoards] intValue];
            self.session.yBoards = [[Common getUserDefaults:keyyBoards] intValue];
            NSString* dimString = [NSString stringWithFormat:@"%i", (self.session.xBoards*30)];
            [settingsDict setValue:dimString forKey:@"width"];
            [settingsDict setValue:[NSString stringWithFormat:@"%i", self.session.yBoards*30] forKey:@"height"];
            
            NSError * err;
            NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:settingsDict options:0 error:&err];
            NSString* settingsString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];//[NSString stringWithString:[writer stringWithObject:settingsDict]];
            urlString = [NSString stringWithFormat:@"%@/SetSettingsBundle/imageId:%@",self.session.serverForCurrentImage, self.session.imageId];
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/vnd.com.munkplast.photopearls.api.SettingsBundle+json" forHTTPHeaderField: @"Content-Type"];
            [request setHTTPBody:[settingsString dataUsingEncoding:NSUTF8StringEncoding]];
            
            settingsBundleConnection = [NSURLConnection connectionWithRequest:request delegate:self];
            
        } else if (connection == settingsBundleConnection) {
            
            urlString = [[NSString alloc] initWithFormat:@"%@/GetPearlifiedImage/imageId:%@", self.session.serverForCurrentImage, self.session.imageId];
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"GET"];
            pearlifiedImageConnection = [NSURLConnection connectionWithRequest:request delegate:self];
            
            
            
        } else if (connection == pearlifiedImageConnection) {
            
            UIImage *img = [UIImage imageWithData:recievedData];
            NSString* imageString = [[NSString alloc] initWithData:recievedData   encoding:NSUTF8StringEncoding];
            self.session.pearlifiedImage = [UIImage imageWithData:recievedData];
            //        [_imgUser setImage:self.session.pearlifiedImage];
            [self.baseDelegate gotPearlifiedImage:img andErrorMsg:nil];
            urlString = [NSString stringWithFormat:@"%@/GetPearlPattern/imageId:%@", self.session.serverForCurrentImage, self.session.imageId];
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"GET"];
            pearlPatternConnection = [NSURLConnection connectionWithRequest:request delegate:self];
            
            
        } else {
            NSString* returnString = [[NSString alloc] initWithData:recievedData encoding: NSUTF8StringEncoding];
            NSData *objectData = [returnString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary* returnDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                       options:NSJSONReadingMutableContainers
                                                                         error:&jsonError];
            NSArray* pearls = [returnDict objectForKey:@"pattern"];
            self.session.thePearls = [[PearlPattern alloc] initWithUglyArray:pearls];
            
            if (self.session.pearlifiedImage == nil) {
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Unable to reach server. Check your internet connection or try later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alertView show];
            }
            
        }
    }
    else if (currentSelection == getShopsConnection) {
        
            NSError *jsonError;
            NSString* storeString = [[NSString alloc] initWithData:recievedData encoding:NSUTF8StringEncoding];
            NSData *objectData = [storeString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary* storeDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&jsonError];
            NSArray* storeArray = [storeDict objectForKey:@"stores"];
            
        [self.baseDelegate receivedData:storeArray anderrorMsg:nil];

    }
    else if (currentSelection == getInspiration) {
        
        NSError *jsonError;
        NSString* urlString = [[NSString alloc] initWithData:recievedData encoding: NSUTF8StringEncoding];
        NSData *objectData = [urlString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* urlDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&jsonError];
        NSString* webAddress = [urlDict objectForKey:@"url"];
        
        if (webAddress == nil) {
            webAddress = @"http://media.munkplast.se/info/goliath_nl";//@"http://www.photopearls.com";
        }
        
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:webAddress];
        
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        //Load the request in the UIWebView.
        [self.baseDelegate receivedData:requestObj anderrorMsg:nil];
    }
    else if (currentSelection == defaultImageReq) {
        NSString *returnString = [[NSString alloc] initWithData:recievedData encoding: NSUTF8StringEncoding];
        [self.baseDelegate receivedData:recievedData anderrorMsg:nil];
    }
    else if (currentSelection == getStore) {
        
        NSError *jsonError;
        NSString* urlString = [[NSString alloc] initWithData:recievedData encoding: NSUTF8StringEncoding];
        NSData *objectData = [urlString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* urlDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&jsonError];
        NSString* webAddress = [urlDict objectForKey:@"url"];
        
        if (webAddress == nil) {
            webAddress = @"https://goliath.services/nl/";//@"http://www.photopearls.com";
        }
        
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:webAddress];
        
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        //Load the request in the UIWebView.
        [self.baseDelegate receivedData:requestObj anderrorMsg:nil];
    }
    
}


-(void)connection:(NSURLConnection *)connection didReceiveAuthenicationChallange:(NSURLAuthenticationChallenge*)challenge {
//    NSURLCredential *credential = [NSURLCredential credentialWithUser:kUserId
//                                                             password:[PHPSession passWd]
//                                                          persistence:NSURLCredentialPersistenceForSession];
//    
        NSURLCredential *credential = [NSURLCredential credentialWithUser:kUserId
                                                                 password:[PHPSession passWd]
                                                              persistence:NSURLCredentialPersistenceForSession];
    
    
    [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError*)terror {
    APILog(@"Ryndurg!");
    
    if (currentSelection == getPearlifiedImage) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Unable to reach server. Check your internet connection or try later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [self.baseDelegate gotPearlifiedImage:nil andErrorMsg:@"error fetching data"];
    }
    else if (currentSelection == getInspiration) {
        NSString* webAddress = @"http://www.photopearls.com";
        
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:webAddress];
        
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        //Load the request in the UIWebView.
        [self.baseDelegate receivedData:requestObj anderrorMsg:nil];
    }
    else if (currentSelection == sendPattern) {
        UIAlertView* alert = [[UIAlertView alloc]
                              initWithTitle:@"Failed to Send Pattern"
                              message: @"Your pattern was not sent. Note that your session will expire after a few hours. Start another project!"
                              delegate:nil cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
   
}

@end
