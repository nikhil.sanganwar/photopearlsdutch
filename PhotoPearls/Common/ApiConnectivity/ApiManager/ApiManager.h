//
//  ApiManager.h
//  Fisho
//
//  Created by Priyesh Das on 3/19/15.
//  Copyright (c) 2015 teks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "PHPSession.h"

@interface ApiManager : NSObject
@property (nonatomic, retain) PHPSession* session;
@property (nonatomic, retain) BaseViewController* baseDelegate;
@property int currentSelection;
# pragma mark
# pragma mark - user
-(void)initWithRequest:(NSMutableURLRequest *)request fromDelegate:(BaseViewController *)delegate ;
@end
