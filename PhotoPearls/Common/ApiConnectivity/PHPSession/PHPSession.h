//
//  PHPSession.h
//  PhotoPearls
//
//  Created by C-H on 2010-10-11.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PearlPattern.h"

#define kNone				0
#define kSelectedImage		1
#define kCroppedImage		2
#define	kPearlifiedImage	3

#define kAppId	@"fb110535155693198"
#define kUserId @"goliath"//@"munkplast-iphone"
#define kPassWd @"yu!sux!Q"

@class BaseViewController;

//@protocol PHPSessionDelegate<NSObject>
//- (void)serverFound:(NSString*)serverName;
//@end

@interface PHPSession : NSObject
@property (strong,nonatomic) UIImage* pearlBox;
@property (nonatomic, retain) UIImage* image;
@property (nonatomic, retain) UIImage* pearlifiedImage;
@property (nonatomic, retain) UIImage* scaledImage;
@property (nonatomic, retain) PearlPattern* thePearls;
@property (nonatomic, assign) int xBoards;
@property (nonatomic, assign) int yBoards;
@property (nonatomic, assign) int logonAttempts;
@property (nonatomic, assign) int serverNumber;
@property (nonatomic, retain) NSString* imageId;
@property (nonatomic, assign) int step;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) BaseViewController* myDelegate;
//@property(nonatomic, retain)id<PHPSessionDelegate> myDelegate;
@property (nonatomic, retain) NSString* serverForCurrentImage;
+(NSString*)server;
-(NSString*)server;
- (void)lookForServer;
- (void)findWorkingServer:(BaseViewController *)delegate;
+ (NSString*)passWd;
@end
