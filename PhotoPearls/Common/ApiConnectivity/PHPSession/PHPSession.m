//
//  PHPSession.m
//  PhotoPearls
//
//  Created by C-H on 2010-10-11.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PHPSession.h"
#import "Common.h"
#import "BaseViewController.h"

@implementation PHPSession
{
    int noOfTries;
}
@synthesize pearlBox;
@synthesize image;
@synthesize pearlifiedImage;
@synthesize scaledImage;
//@synthesize thePearls;
@synthesize xBoards, yBoards;
@synthesize logonAttempts;
@synthesize serverNumber;
@synthesize imageId;
@synthesize step;
@synthesize name;
@synthesize myDelegate;
@synthesize serverForCurrentImage;

+(NSString*)server {
	int serverNo = arc4random()%26 + 97;
	NSString* urlString = [NSString stringWithFormat:@"https://%C.api.photopearls.se/v1.1", (unichar)serverNo];
	NSLog(@"%@", urlString);
	//return @"http://s00.hajrabarber.se:8080/photopearls-server";
	//return @"https://s00.hajrabarber.se/v1.0";
	//return @"https://testing.api.photopearls.se/v1.0";
	return urlString;
}

- (NSString*)server {
	return [PHPSession server];
}

-(void)lookForServer {
	NSString* thisServer;
	switch (logonAttempts) {
		case 0:
			serverNumber = arc4random()%26 + 97;
			logonAttempts++;
			break;
		case 1:
			serverNumber = (serverNumber + 13) % 26;
			logonAttempts++;
			break;
		case 2:	
			serverNumber = (serverNumber + 6) % 26;
			logonAttempts++;
			break;
		case 3:
			serverNumber = (serverNumber + 13) % 26;
			logonAttempts++;
			break;
		case 4:
			serverNumber = (serverNumber + 3) % 26;
			logonAttempts++;
			break;
		case 5:
			serverNumber = (serverNumber + 13) % 26;
			logonAttempts++;
			break;
		default:
			//[myDelegate serverFound:nil];
			return;
			break;
		}
	thisServer = [NSString stringWithFormat:@"https://%C.api.photopearls.se/v1.1", (unichar)serverNumber];
	NSString* urlString = [[NSString alloc] initWithFormat:@"%@/EchoHeaders", thisServer];
	NSMutableURLRequest	*request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
 	[request setHTTPMethod:@"GET"];
	[NSURLConnection connectionWithRequest:request delegate:self];
}


- (void)findWorkingServer:(BaseViewController *)delegate {
    APILog(@"hi");
	if (delegate != nil) {
        noOfTries = 1;
		logonAttempts = 0;
		myDelegate = delegate;
		[self lookForServer];
	} 
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{

}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    if (noOfTries == 5) {
        [self.myDelegate serverFound:nil];
    }
    else {
        [self lookForServer];
        noOfTries++;
    }
	
	/*
	NSLog(@"Error: %i : ", error.code, error.domain);
	UIAlertView* alertView = [[UIAlertView alloc] 
							  initWithTitle:@"Server Error" 
							  message:@"The application has trouble reaching its server. You may proceed, but please note that this app is dependent on server access." 
							  delegate:nil 
							  cancelButtonTitle:@"OK" 
							  otherButtonTitles:nil];
	[alertView show];
	[alertView release];
	 */
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSString* thisServer = [NSString stringWithFormat:@"https://%C.api.photopearls.se/v1.1", (unichar)serverNumber];
	[self.myDelegate serverFound:thisServer];
    
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return YES; 
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
		NSURLCredential *credential = [NSURLCredential credentialWithUser:kUserId
                                                             password:[PHPSession passWd]
														  persistence:NSURLCredentialPersistenceForSession];
	
	[[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
	
}

+ (NSString*)passWd {
	//NSString* realPassWd = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@", @"u", @"b", @"6", @"P", @"*", @"x", @"!", @"Q"];//ub6P*x!Q
    NSString* realPassWd = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@", @"d", @"!", @"9", @"1", @"a", @"E", @"W", @"2", @"*"];//d!91aEW2*

	return realPassWd;
}

/*
#pragma mark NSEncoding
-(void)encodeWithCoder:(NSCoder*)encoder {
	NSData* imageData;

	if (pearlifiedImage != nil) {
		imageData = [NSData dataWithData:UIImagePNGRepresentation(pearlifiedImage)];
		[encoder encodeObject:imageData forKey:@"image"];
	} else if (scaledImage != nil) {
		imageData = [NSData dataWithData:UIImagePNGRepresentation(scaledImage)];
		[encoder encodeObject:imageData forKey:@"image"];
	} else if (image != nil) {
		imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
		[encoder encodeObject:imageData forKey:@"image"];
	}
	
	[encoder encodeInt:xBoards forKey:@"xBoards"];
	[encoder encodeInt:yBoards forKey:@"yBoards"];
	
	if (thePearls != nil) {
		[encoder encodeObject:thePearls forKey:@"thePearls"];
	}
	
	[encoder encodeInt:step forKey:@"step"];
	
	//[encoder encodeObject:name forKey:@"name"];

}

- (id)initWithCoder:(NSCoder *)aDecoder {
	if (self = [super init]) {
		step = [aDecoder decodeIntForKey:@"step"];
		switch (step) {
			case kNone:
			
			break;
		case kSelectedImage:
			image = [aDecoder decodeObjectForKey:@"image"];
				
			break;
		case kCroppedImage:
			scaledImage = [aDecoder decodeObjectForKey:@"image"];
			break;
		case kPearlifiedImage:
			pearlifiedImage = [aDecoder decodeObjectForKey:@"image"];
			thePearls = [aDecoder decodeObjectForKey:@"thePearls"];
			break;
		default:
			break;
		}
	}
	return self;
}

-(id)copyWithZone:(NSZone *)zone {
	NSData* imageData;
	PHPSession* copy = [[[self class] allocWithZone:zone] init];
	
	imageData = [NSData dataWithData:UIImagePNGRepresentation(self.image)];
	copy.image = [[[UIImage allocWithZone:zone] initWithData: imageData] autorelease];
	
	imageData = [NSData dataWithData:UIImagePNGRepresentation(self.scaledImage)];
	copy.scaledImage = [[[UIImage allocWithZone:zone] initWithData: imageData] autorelease];
	
	imageData = [NSData dataWithData:UIImagePNGRepresentation(self.pearlifiedImage)];
	copy.pearlifiedImage = [[[UIImage allocWithZone:zone] initWithData: imageData] autorelease];
	
	copy.step = self.step;
	copy.xBoards = self.xBoards;
	copy.yBoards = self.yBoards;
	copy.thePearls = [self.thePearls copyWithZone:zone];
	copy.name = [self.name copyWithZone:zone];
	copy.imageId = [self.imageId copyWithZone:zone];
	
	return copy;
} */
@end
