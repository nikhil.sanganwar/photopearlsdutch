//
//  FacebookHandler.h
//  Bender
//
//  Created by Mahesh Agrawal on 7/13/15.
//  Copyright (c) 2015 Teknowledge Software. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

typedef enum {
    kFacebookStatusSuccess = 1,
    kFacebookStatusFailed = 2,
    kFacebookStatusCancelled = 3
}FacebookStatus;

@interface FacebookHandler : NSObject

+ (BOOL)isLoggedIn;

+ (void)loginToFacebookWithSuccess:(void (^)(FacebookStatus status, NSError *error))success;

+ (void)fetchUserProfileWithSuccess:(void(^)(FacebookStatus status, id response, NSError *fberror))success;

+ (void)fetchAlbumsOfUserWithLimit:(int)limit withPage:(NSString*)after withSuccess:(void (^)(FacebookStatus status, id response, NSString *after))success;

+ (void)fetchPhotosOfAlbumWithID:(NSString*)albumID withLimit:(int)limit withPage:(NSString*)after withSuccess:(void (^)(FacebookStatus status, id response, NSString *after))success;

@end
