//
//  FacebookHandler.m
//  Bender
//
//  Created by Mahesh Agrawal on 7/13/15.
//  Copyright (c) 2015 Teknowledge Software. All rights reserved.
//

#import "FacebookHandler.h"

@implementation FacebookHandler

+ (BOOL)isLoggedIn
{
    return ([FBSDKAccessToken currentAccessToken])?YES:NO;
}

+ (void)loginToFacebookWithSuccess:(void (^)(FacebookStatus status, NSError *error))success{
    FBSDKLoginManager *login = [FBSDKLoginManager new];
    login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    dispatch_async(dispatch_get_main_queue(), ^{
        [login logInWithReadPermissions:@[@"email",@"public_profile",@"user_photos"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                success(kFacebookStatusFailed, error);
            } else if (result.isCancelled) {
                success(kFacebookStatusCancelled, nil);
            } else {
                success(kFacebookStatusSuccess, nil);
            }
        }];
    });
}

+ (void)fetchUserProfileWithSuccess:(void(^)(FacebookStatus status, id response, NSError *fberror))success
{
    if ([FBSDKAccessToken currentAccessToken]) {
        NSDictionary *dicParameters = [NSDictionary dictionaryWithObjects:@[@"id,name,email,first_name,last_name,work"] forKeys:@[@"fields"]];
        dispatch_async(dispatch_get_main_queue(), ^{
//            FBSDKLoginManager *login = [FBSDKLoginManager new];
//            login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
//            [login logInWithReadPermissions:@[@"email",@"public_profile",@"user_photos"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//                if (error) {
//                    success(kFacebookStatusFailed, nil, error);
//                } else if (result.isCancelled) {
//                    success(kFacebookStatusCancelled, nil, nil);
//                } else {
            
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"/me"] parameters:dicParameters] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                        if (error) {
                            success(kFacebookStatusFailed, nil, error);
                        }else{
                            //NSString *userID = [result valueForKey:@"id"];
                            //NSString *userName = [result valueForKey:@"name"];
                            //userName = [userName stringByReplacingOccurrencesOfString:@" " withString:@""];
                            //NSString *profilePic = [NSString stringWithFormat: @"http://graph.facebook.com/%@/picture?type=large", userID];
                            //NSDictionary *dicUser = [NSDictionary dictionaryWithObjects:@[userID, userName, profilePic] forKeys:@[@"id",@"username",@"image"]];
                            
                            success(kFacebookStatusSuccess, result, nil);
                        }
                    }];
//                }
//            }];
            
        });
    } else {
        [self loginToFacebookWithSuccess:^(FacebookStatus status, NSError *error) {
            if (status == kFacebookStatusSuccess) {
                [self fetchUserProfileWithSuccess:success];
            }else{
                success(status, nil, error);
            }
        }];
    }
}

+ (void)fetchAlbumsOfUserWithLimit:(int)limit withPage:(NSString*)after withSuccess:(void (^)(FacebookStatus status, id response, NSString *after))success{
    if ([FBSDKAccessToken currentAccessToken]) {
        NSDictionary *dicParameters = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d",limit],after,@"id,name"] forKeys:@[@"limit",@"after",@"fields"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"%@/albums",[FBSDKAccessToken currentAccessToken].userID] parameters:dicParameters] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    success(kFacebookStatusFailed, nil, @"");
                }else{
                    NSArray *arrData = [result objectForKey:@"data"];
                    success(kFacebookStatusSuccess, arrData, [[[result objectForKey:@"paging"] objectForKey:@"cursors"] valueForKey:@"after"]);
                }
            }];
        });
    } else {
        [self loginToFacebookWithSuccess:^(FacebookStatus status, NSError *error) {
            if (status == kFacebookStatusSuccess) {
                [self fetchAlbumsOfUserWithLimit:limit withPage:after withSuccess:success];
            }else{
                success(status, nil, @"");
            }
        }];
    }
}

+ (void)fetchPhotosOfAlbumWithID:(NSString*)albumID withLimit:(int)limit withPage:(NSString*)after withSuccess:(void (^)(FacebookStatus status, id response, NSString *after))success{
    if ([FBSDKAccessToken currentAccessToken]) {
        NSDictionary *dicParameters = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d",limit],after,@"id,images,height"] forKeys:@[@"limit",@"after",@"fields"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"%@/photos?type=normal",albumID] parameters:dicParameters] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    success(kFacebookStatusFailed, nil, @"");
                }else{
                    NSArray *arrData = [result objectForKey:@"data"];
                    NSMutableArray *arrPhotos = [[NSMutableArray alloc]init];
                    for (NSDictionary *dicPhoto in arrData) {
                        NSDictionary *dicNewPhoto = [NSDictionary dictionaryWithObjects:@[[[[dicPhoto objectForKey:@"images"] objectAtIndex:0] valueForKey:@"source"],@"no",@"no"] forKeys:@[@"url",@"isvideo",@"issound"]];
                        [arrPhotos addObject:dicNewPhoto];
                    }
                    success(kFacebookStatusSuccess, [NSArray arrayWithArray:arrPhotos], [[[result objectForKey:@"paging"] objectForKey:@"cursors"] valueForKey:@"after"]);
                }
            }];
        });
    } else {
        [self loginToFacebookWithSuccess:^(FacebookStatus status, NSError *error) {
            if (status == kFacebookStatusSuccess) {
                [self fetchAlbumsOfUserWithLimit:limit withPage:after withSuccess:success];
            }else{
                success(status, nil, @"");
            }
        }];
    }
}

@end
