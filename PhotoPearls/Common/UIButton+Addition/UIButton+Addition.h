//
//  UIButton+Addition.h
//  TeamCommish
//
//  Created by Priyesh Das on 10/14/15.
//  Copyright © 2015 Teknowledge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Addition)
-(void)addHighlightColor ;

@end
