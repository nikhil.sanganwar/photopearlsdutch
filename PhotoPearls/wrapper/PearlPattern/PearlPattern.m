//
//  PearlPattern.m
//  PhotoPearls
//
//  Created by C-H on 2010-10-18.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PearlPattern.h"
#import "PearlPile.h"

@implementation PearlPattern
@synthesize rows;

-(id)initWithUglyArray:(NSArray*)array {
	
	
	NSMutableArray* newPattern = [NSMutableArray array];
	for (int i = 0; i < array.count; i++) {
		NSMutableArray* tempRow = [NSMutableArray array];
		NSArray* arrayRow = [array objectAtIndex:i];
		for (int j = 0; j < arrayRow.count; j++) {
			int pearl = [(NSNumber*)[arrayRow objectAtIndex:j]intValue];
			[tempRow addObject:[NSNumber numberWithInt:pearl-1]];
		}
		[newPattern addObject:tempRow];
	}
	//rows = newPattern;
	 
	//NSMutableArray* newPattern = [[NSMutableArray alloc] initWithArray:array];
    rows = newPattern;
	return self;
}

-(id)initWithSize:(int)xSize :(int)ySize{
	NSMutableArray* tempRows = [[NSMutableArray alloc]init];
	NSMutableArray* tempCol;
	for (int i = 0; i<ySize; i++) {
		tempCol = [NSMutableArray array];
		for (int j = 0; j < xSize; j++) {
			//UsableInt* tempInt = [[UsableInt alloc]initWithInt:14];
			NSNumber* tempInt = [NSNumber numberWithInt:15];
			[tempCol addObject:tempInt];
		}
		[tempRows addObject:tempCol];
	}
	rows = tempRows;
	//[tempRows release];
	return self;
}

-(void)set:(int)x :(int)y : (int)v{
	NSMutableArray* theRow = (NSMutableArray*)[rows objectAtIndex:y];
	//UsableInt* thePearl = (UsableInt*)[theRow objectAtIndex:x];
	//[thePearl setValue:v];
	[theRow replaceObjectAtIndex:x withObject: [NSNumber numberWithInt:v]];
}

-(int)get:(int)x :(int)y {
	NSArray* theRow = (NSArray*)[rows objectAtIndex:y];
	//UsableInt* thePearl = (UsableInt*)[theRow objectAtIndex:x];
	NSNumber* thePearl = [theRow objectAtIndex:x];
	int result = [thePearl intValue];
	//[theRow release];
	//[thePearl release];
	return result;
}

// Returns NSArray of length 31 with NSNumbers. At index 30 is total pearl count.
-(NSArray*)pearlCount {
	NSMutableArray* resultArray = [NSMutableArray array];
	//NSMutableArray* countArray = [[NSMutableArray alloc] init];
	
	int countArray[31];
	
	for (int i = 0; i < 31; i++) {
		//UsableInt* temp = [[UsableInt alloc] initWithInt:0];
		//NSNumber* temp = [[NSNumber numberWithInt:0];
		//[countArray addObject:temp];
		//[temp release];
		countArray[i] = 0;
	}
	
	for (NSArray* tempRow in rows) {
		for (NSNumber* tempInt in tempRow) {
			//[(UsableInt*)[countArray objectAtIndex: [tempInt intValue]+1] addInt:1];
			countArray[[tempInt intValue]]++;
			countArray[30]++;
		}
	}
	
	for (int i = 0; i < 31; i++) {
		if (countArray[i] > 0) {
			PearlPile* newPile = [[PearlPile alloc] init];
			newPile.color = i+1;
			newPile.numberOfPearls = countArray[i];
			[resultArray addObject:newPile];
			//[newPile release];
		} 
		/*
		NSNumber* tempNumber = [NSNumber numberWithInt: countArray[i]];
		[resultArray addObject:tempNumber];
		 */
	}
		
	return resultArray;
}

-(UIImage*)generateThumbnail {
	
	return nil;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
	[aCoder encodeObject:rows forKey:@"rows"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super init];
	rows = [aDecoder decodeObjectForKey:@"rows"];
	return self;
}

- (id)copyWithZone:(NSZone *)zone {
	PearlPattern* copy = [[[self class] allocWithZone:zone] init];
	copy.rows = [self.rows copyWithZone:zone];
	return copy;
}
@end
