//
//  PearlPattern.h
//  PhotoPearls
//
//  Created by C-H on 2010-10-18.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "UsableInt.h"

@interface PearlPattern : NSObject <NSCoding, NSCopying> {
	NSMutableArray* rows;
	int columns;
}
@property (nonatomic, retain) NSArray* rows;
-(id)initWithPearlPattern:(PearlPattern*)pearlPattern;
-(id)initWithUglyArray:(NSArray*)array;
-(id)initWithSize:(int)xSize :(int)ySize;
-(void)set:(int)x : (int)y : (int)v;
-(int)get:(int)x : (int)y;

-(NSArray*)pearlCount;
-(UIImage*)generateThumbnail;

@end
