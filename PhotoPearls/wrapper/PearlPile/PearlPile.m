//
//  PearlPile.m
//  PhotoPearls
//
//  Created by C-H on 2011-03-03.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PearlPile.h"

@implementation PearlPile
@synthesize color;
@synthesize numberOfPearls;

- (int)extraPearlContainers {
	return [self extraPearlContainerWithPearlsInStarterKit:500 andPearlsInRefill:3500];
	}

- (int)extraPearlContainerWithPearlsInStarterKit: (int)pearlsInStarterKit andPearlsInRefill: (int)pearlsInRefill {
    
    if (self.numberOfPearls < pearlsInStarterKit) {
		return 0;
	}
	
	int result = ((self.numberOfPearls - pearlsInStarterKit) / pearlsInRefill);
	if ((self.numberOfPearls - pearlsInStarterKit) % pearlsInRefill != 0) {
		result++;
	} 
	return result;

    
}
@end
