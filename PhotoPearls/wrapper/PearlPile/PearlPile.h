//
//  PearlPile.h
//  PhotoPearls
//
//  Created by C-H on 2011-03-03.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PearlPile : NSObject {
	int color;
	int numberofPearls;
}
@property (nonatomic) int color;
@property (nonatomic) int numberOfPearls;
-(int)extraPearlContainers;
-(int)extraPearlContainerWithPearlsInStarterKit: (int)pearlsInStarterKit andPearlsInRefill: (int)pearlsInRefill;
@end
