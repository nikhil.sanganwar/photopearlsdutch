//
//  ShoppingListController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 11/2/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "ShoppingListController.h"
#import "ShoppingListCell.h"
#import "PearlPile.h"

@interface ShoppingListController ()
@property (weak, nonatomic) IBOutlet UILabel *lblCreatePatternText;
@property (weak, nonatomic) IBOutlet UILabel *lblBeadsUsed;
@property (weak, nonatomic) IBOutlet UILabel *lblStarterKit;
@property (weak, nonatomic) IBOutlet UITableView *tblBottle;
@property (weak, nonatomic) IBOutlet UITableView *tblBag;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeadingFirstView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeadingSecondView;
@property (strong,nonatomic) NSArray* countArray;
@property (strong,nonatomic) PearlPile* thisPile;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightLabelView;
@property (weak, nonatomic) IBOutlet UIView *vwFirst;
@property (weak, nonatomic) IBOutlet UIView *vwSecond;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeadingBlueView;
@property (weak, nonatomic) IBOutlet UIButton *btnBag;
@property (nonatomic, retain) UISegmentedControl* boxTypeSelector;
@end

@implementation ShoppingListController
{
    BOOL btnBottleSelected;
    int extraBoards;
    int extraPearls;
    //int selector;
}

@synthesize countArray;
@synthesize thisPile;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self addNavBarWithTitle:@"LISTA DE COMPRAS" andLeftButton:@"back" andRightButton:@"" onView:self.view];
     //bynikhil4417
    
    [self addNavBarWithTitle:@"LIJST MET BENODIGDHEDEN" andLeftButton:@"back" andRightButton:@"" onView:self.view withSize:15.0];

    self.title = @"LISTE D`ACHATS";
}

-(void)viewDidAppear:(BOOL)animated {
    [self setupView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupView {
    btnBottleSelected = YES;
    countArray = [self.session.thePearls pearlCount];
    //[self.tblBag reloadData];
    if (self.session.xBoards * self.session.yBoards <= 4) {
        extraBoards = 0;
    } else {
        extraBoards = 2;
    }

    _lblStarterKit.text = @"Eén PhotoPearls starter kit";
    /*
     extraPearls = 0;
     
     for (PearlPile* pile in countArray) {
     if (pile.color != 31) {
     extraPearls += [pile extraPearlContainers];
     }
     }
     */
    
    int extra = 0;
    extraPearls = 0;
    
    for (thisPile in countArray) {
        if (thisPile.color != 31) {
            switch (0) {
                case 0:
                    extra = [thisPile extraPearlContainerWithPearlsInStarterKit:500
                                                              andPearlsInRefill:3500];
                    break;
                case 1:
                    extra = [thisPile extraPearlContainerWithPearlsInStarterKit:500
                                                              andPearlsInRefill:1100];
                    break;
                default:
                    extra = [thisPile extraPearlContainerWithPearlsInStarterKit:5
                                                              andPearlsInRefill:10];
                    break;
                    
            }
            
            extraPearls += extra;
            
        }
    }
    [self.tblBottle reloadData];
}
- (IBAction)btnBottleAction:(id)sender {
    btnBottleSelected = YES;
    //selector = 0;
    [self.tblBottle reloadData];
    [self.view layoutIfNeeded];
   _constraintLeadingFirstView.constant = 0;
    _constraintLeadingBlueView.constant = 0;
    //_constraintLeadingSecondView.constant = _constraintLeadingSecondView.constant + _vwSecond.frame.size.width;
    _vwFirst.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded]; // Called on parent view
    } completion:^(BOOL finished) {
        _vwSecond.hidden = YES;
    }];
}
- (IBAction)btnBagAction:(id)sender {
    
    btnBottleSelected = NO;
    //selector = 1;
    [self.tblBag reloadData];
    [self.view layoutIfNeeded];
    _constraintLeadingFirstView.constant = _constraintLeadingFirstView.constant - self.view.frame.size.width;
    _constraintLeadingBlueView.constant = _btnBag.frame.origin.x;
    //_constraintLeadingSecondView.constant = _constraintLeadingSecondView.constant - _vwSecond.frame.size.width;
    _vwSecond.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded]; // Called on parent view
    } completion:^(BOOL finished) {
        _vwFirst.hidden = YES;
    }];
}

#pragma mark
#pragma mark Table View methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int result;
    switch (section) {
            case 0:
            return 1;
        case 1:
            result = 1;
            if (extraBoards > 0) {
                result++;
            }
            if (extraPearls > 0) {
                result++;
            }
            return result;
            break;
        case 2:
            return [countArray count] + 1;
            break;
        default:
            return 0;
            break;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ShoppingListCell";
    static NSString *cellIdentifier2 = @"ShoppingProject";
//    ShoppingListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ShoppingListCell class]) forIndexPath:indexPath];
    ShoppingListCell *cell;
    if (indexPath.section == 0) {
        cell = (ShoppingListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
        cell.lblTotal.hidden = YES;
    }else if (indexPath.section == 1) {
        cell = (ShoppingListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
    }
    else {
        cell = (ShoppingListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    }
    [cell layoutIfNeeded];

//    if (!cell) {
//        cell = (ShoppingListCell *)[[UITableViewController alloc] initWithNibName:cellIdentifier bundle:nil];
//    }
    int extra;
    
    if (indexPath.section == 1) {
        extraPearls = 0;
        
        for (PearlPile* thisPile1 in countArray) {
            if (thisPile1.color != 31) {
                
//                if (btnBottleSelected) {
//                    selector = 0;
//                }
//                else {
//                    selector = 1;
//                }
                switch ([_boxTypeSelector selectedSegmentIndex ]) {
                    case 0:
                        extra = [thisPile1 extraPearlContainerWithPearlsInStarterKit:500
                                                                  andPearlsInRefill:3500];
                        break;
                    case 1:
                        extra = [thisPile1 extraPearlContainerWithPearlsInStarterKit:500
                                                                  andPearlsInRefill:1100];
                        break;
                    default:
                        extra = [thisPile1 extraPearlContainerWithPearlsInStarterKit:5
                                                                  andPearlsInRefill:10];
                        break;
                        
                }
                
                extraPearls += extra;
                
            }
        }
        
    }
    
    
    NSString* refillType;
    if (_boxTypeSelector.selectedSegmentIndex == 0) {
        refillType = @"bottles";
    } else {
        refillType = @"bags";
    }
    
    switch (indexPath.section) {
        case 0:
            cell.backgroundColor = UIColor.groupTableViewBackgroundColor;
            if(_boxTypeSelector == nil){
                NSArray* titles = [NSArray arrayWithObjects:@"FLESSEN",@"ZAKJES", nil];
                UISegmentedControl* selector = [[UISegmentedControl alloc]initWithItems:titles];
                selector.frame = CGRectMake(10, 0, cell.frame.size.width - 20, 46);
                selector.selectedSegmentIndex = 0;
                [selector addTarget:self
                             action:@selector(typeChanged)
                   forControlEvents:UIControlEventValueChanged];
                selector.backgroundColor = UIColor.whiteColor;
                _boxTypeSelector = selector;
                cell.myControl = selector;
                // [selector release];
                
            }
            [cell addSubview:_boxTypeSelector];
            break;
        case 1:
            cell.imgBeadType.hidden = YES;
            cell.lblBeadCount.hidden = YES;
            switch (indexPath.row) {
                    
                case 0:
                    cell.lblTotal.text = @"Eén PhotoPearls starter kit";
                    break;
                case 1:
                    if (extraBoards > 0) {
                        cell.lblTotal.text = [NSString stringWithFormat:@"%i extra grondplaten", extraBoards];
                    } else {
                        cell.lblTotal.text = [NSString stringWithFormat:@"%i navulflessen kralen/navulzakjes krale zie onder",
                                               extraPearls];
                        
                    }
                    break;
                case 2:
                    cell.lblTotal.text = [NSString stringWithFormat:@"%i navulflessen kralen/navulzakjes krale zie onder",
                                           extraPearls];
                    break;
                default:
                    cell.lblTotal.text = @"Error!";
                    break;
            }
            break;
        case 2:
            cell.imgBeadType.hidden = NO;
            cell.lblBeadCount.hidden = NO;
//            NSInteger totalRow = [tableView numberOfRowsInSection:indexPath.section];
//
            
            if (indexPath.row == [countArray count]) {
                cell.imgBeadType.hidden = YES;
                cell.lblBeadCount.hidden = YES;
                cell.lblTotal.hidden = YES;
                cell.backgroundColor = self.mainContainer.backgroundColor;
            }
            else {
                cell.backgroundColor = [UIColor whiteColor];
                thisPile = [countArray objectAtIndex:indexPath.row];
                if (thisPile.color == 31) {
                    cell.lblTotal.hidden = NO;
                    cell.imgBeadType.hidden = YES;
                    cell.lblTotal.text = [NSString stringWithFormat: @"Totaal"];
                    cell.lblBeadCount.text = [NSString stringWithFormat:@"%i Kraal", [thisPile numberOfPearls]];
                    
                }else {
                    cell.imgBeadType.hidden = NO;
                    cell.lblTotal.hidden = YES;
                    UIImage* thisPearlImage = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", thisPile.color]]; //[pearlBoxArray objectAtIndex:(thisPile.color-1)];
                    cell.imgBeadType.image = thisPearlImage;
                    int extra;
                    switch ([_boxTypeSelector selectedSegmentIndex]) {
                        case 0:
                            extra = [thisPile extraPearlContainerWithPearlsInStarterKit:500
                                                                      andPearlsInRefill:3500];
                            break;
                        case 1:
                            extra = [thisPile extraPearlContainerWithPearlsInStarterKit:500
                                                                      andPearlsInRefill:1100];
                            break;
                        default:
                            extra = [thisPile extraPearlContainerWithPearlsInStarterKit:5
                                                                      andPearlsInRefill:10];
                            break;
                    }
                    if (extra == 1) {
                        cell.lblTotal.text = [NSString stringWithFormat:@"          +%i Navulling", extra];
                        cell.lblTotal.hidden = NO;
                    } else if (extra > 1) {
                        cell.lblTotal.text = [NSString stringWithFormat:@"          +%i Navulling", extra];
                        cell.lblTotal.hidden = NO;
                    }
                    cell.lblBeadCount.text = [NSString stringWithFormat:@"%i Kraal", [thisPile numberOfPearls]];
                }
            }
            
            break;
            
        default:
            break;
    }

    
//        thisPile = [countArray objectAtIndex:indexPath.row];
//        
//        if (thisPile.color == 31) {
//            cell.imgBeadType.hidden = YES;
//            cell.lblTotal.hidden = NO;
//            cell.lblBeadCount.text = [NSString stringWithFormat:@"%d",thisPile.numberOfPearls];
//        }
//        else {
//            int extra;
//            if (btnBottleSelected) {
//                extra = [thisPile extraPearlContainerWithPearlsInStarterKit:500
//                                                          andPearlsInRefill:3500];
//            }
//            else {
//                extra = [thisPile extraPearlContainerWithPearlsInStarterKit:500
//                                                          andPearlsInRefill:1100];
//            }
//            
//            if (extra == 1) {
//                cell.lblTotal.text = [NSString stringWithFormat:@"          +%i refill", extra];
//                cell.lblTotal.hidden = NO;
//            } else if (extra > 1) {
//                cell.lblTotal.text = [NSString stringWithFormat:@"          +%i refills", extra];
//                cell.lblTotal.hidden = NO;
//            }
//            else if (extra == 0) {
//                cell.lblTotal.hidden = YES;
//            }
//            
//            cell.imgBeadType.hidden = NO;
//            cell.imgBeadType.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", thisPile.color]]; //[pearlBoxArray objectAtIndex:(thisPile.color-1)];
//            cell.lblBeadCount.text = [NSString stringWithFormat:@"%d",thisPile.numberOfPearls];
//        }
    
    

    return cell;

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"KIES NAVULVERPAKKING";
            break;
        case 1:
            return @"OM JE PATROON TE MAKEN";
            break;
        case 2:
            return @"GEBRUIKTE KRALEN, OP KLEUR GESORTEERD";
            break;
        default:
            return @"KOMKOMMER!";
            break;
    }
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
//    // Background color
    view.tintColor = self.mainContainer.backgroundColor;
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor blackColor]];
    //header.textLabel.text = [header.textLabel.text capitalizedString];
    header.textLabel.font = [UIFont systemFontOfSize:17];
    // Another way to set the background color
    // Note: does not preserve gradient effect of original header
    // header.contentView.backgroundColor = [UIColor blackColor];
}

-(void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    // Background color
    view.tintColor = self.mainContainer.backgroundColor;
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *tempView=[[UIView alloc]initWithFrame:tableView.frame];
//    tempView.backgroundColor=[UIColor clearColor];
//    
//    UILabel *tempLabel=[[UILabel alloc]initWithFrame:tempView.frame];
//    tempLabel.backgroundColor=[UIColor clearColor];
//    tempLabel.shadowColor = [UIColor blackColor];
//    tempLabel.shadowOffset = CGSizeMake(0,2);
//    tempLabel.textColor = [UIColor redColor]; //here you can change the text color of header.
//    tempLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
//    tempLabel.font = [UIFont boldSystemFontOfSize:14];
//    tempLabel.text=@"Header Text";
//    [tempView addSubview:tempLabel];
//    return tempView;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 25.0;
            break;
        default:
            return 0.0;
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return 52;
    }
    else {
        return 48;
    }
    
}

- (void)typeChanged {
    
    [_tblBottle reloadData];
}

@end
