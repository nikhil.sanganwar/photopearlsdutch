//
//  WebElementsDisplayController.h
//  PhotoPearls
//
//  Created by Priyesh Das on 11/5/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface WebElementsDisplayController : BaseViewController
@property (nonatomic, retain) NSString* urlAddress;
@property (assign) BOOL isStoreLocatorDisplay;

@end
