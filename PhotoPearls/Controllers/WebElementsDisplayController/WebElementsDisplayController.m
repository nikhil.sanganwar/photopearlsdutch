//
//  WebElementsDisplayController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 11/5/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "WebElementsDisplayController.h"

@interface WebElementsDisplayController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation WebElementsDisplayController
@synthesize webView;
@synthesize urlAddress;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_isStoreLocatorDisplay) {
//        [self addNavBarWithTitle:@"ENCUENTRA TIENDAS" andLeftButton:@"back" andRightButton:@"" onView:self.view];
//        self.title = @"TROUVER UN MAGASIN";
        //bynikhil4417
        [self addNavBarWithTitle:@"ZOEK EEN WINKEL" andLeftButton:@"back" andRightButton:@"" onView:self.view];
        self.title = @"TROUVER UN MAGASIN";
    
    }
    else {
        //[self addNavBarWithTitle:@"INSPIRATE" andLeftButton:@"back" andRightButton:@"" onView:self.view];
         //bynikhil4417
        [self addNavBarWithTitle:@"INSPIRATIE" andLeftButton:@"back" andRightButton:@"" onView:self.view];
        
        self.title = @"SUR PHOTOPEARLS";
    }
    
    
    if (checkInternetConnection()) {
        if (self.urlAddress == nil) {
            [self.session findWorkingServer:self];
        } else {
            //Create a URL object.
            NSURL *url = [NSURL URLWithString:urlAddress];
            
            //URL Requst Object
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            
            //Load the request in the UIWebView.
            [webView loadRequest:requestObj];
            webView.delegate=self;
        }
//        NSString *strurl;
//        if (_isStoreLocatorDisplay)
//        {
//            strurl = [NSString stringWithFormat:@"https://goliath.services/nl/"];
//
//           
//        }
//        else
//        {
//            strurl = [NSString stringWithFormat:@"http://media.munkplast.se/info/goliath_nl"];
//
//        }
//        
//        NSURL *url = [NSURL URLWithString:strurl];
//        
//        //URL Requst Object
//        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//        
//        //Load the request in the UIWebView.
//        [webView loadRequest:requestObj];
//        webView.delegate=self;
        
        
    }
    else {
        [Common showToastMessage:keyOfflineMessage withTitle:@"Connection Error" withAction:YES withMessageType:NO];
    }

    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height/2.0);
    //[activityView setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size, <#CGFloat height#>)];
    [activityView startAnimating];
    activityView.tag = 101;
    [self.view addSubview:activityView];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
   // [self.view viewWithTag:101].hidden = YES;
    UIView *viewToRemove = [self.view viewWithTag:101];
    [viewToRemove removeFromSuperview];


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)serverFound:(NSString *)serverName {
    if (serverName == nil) {
        
    } else {
        NSLocale* locale = [NSLocale currentLocale];
        NSString* localeID = [locale objectForKey:NSLocaleCountryCode];
        NSString* languageID = [locale objectForKey:NSLocaleLanguageCode];
//        NSString* urlString = [NSString stringWithFormat:@"%@/GetInspirationByCountryAndLanguage/country:%@/language:%@", serverName, localeID, languageID];
        NSString* urlString;
        if (_isStoreLocatorDisplay) {
            urlString = [NSString stringWithFormat:@"https://goliath.services/nl/"];
            self.apiManager.currentSelection = getStore;
        }
        else {
            urlString = [NSString stringWithFormat:@"http://media.munkplast.se/info/goliath_nl"];
            self.apiManager.currentSelection = getInspiration;
        }
        

        NSMutableURLRequest	*request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"GET"];
        
        [self.apiManager initWithRequest:request fromDelegate:self];
        
    }
}


-(void)receivedData:(id)data anderrorMsg:(NSString *)error{
    NSURLRequest *urlReq = (NSURLRequest *)data;
    webView.delegate = self;
    [webView loadRequest:urlReq];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error : %@",error);
}

@end
