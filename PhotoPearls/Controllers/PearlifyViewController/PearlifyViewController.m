//
//  PearlifyViewController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 10/30/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "PearlifyViewController.h"
#import "ResultViewController.h"

@interface PearlifyViewController ()
@property (weak, nonatomic) IBOutlet ButtonView *vwPearlify;
@property (weak, nonatomic) IBOutlet ButtonView *vwNextStep;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong,nonatomic)NSURLConnection* imageConnection;
@property (strong,nonatomic)NSMutableData* recievedData;
@property (strong,nonatomic)MBProgressHUD *hud;
@property (weak, nonatomic) IBOutlet ButtonView *vwImgeUser;
@end

@implementation PearlifyViewController
{
    NSURLConnection* settingsBundleConnection;
    NSURLConnection* pearlifiedImageConnection;
    NSURLConnection* pearlPatternConnection;
    MBProgressHUD *hud;
    UIActivityIndicatorView *vwSpinner;
}
@synthesize imageConnection;
@synthesize recievedData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupView];
    
    //--
    _vwNextStep.alpha = 0.5;
    _vwNextStep.userInteractionEnabled = NO;
    [_vwImgeUser setCornerRadius:5.0];
}
-(void)viewDidAppear:(BOOL)animated {
    vwSpinner.center = _imgUser.center;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupView {
//    [self addNavBarWithTitle:@"PEARLIFIE" andLeftButton:@"back" andRightButton:@"3" onView:self.view];
//    self.title = @"PEARLIFIE";
    [self addNavBarWithTitle:@"  OMZETTEN NAAR KRALEN  " andLeftButton:@"back" andRightButton:@"3" onView:self.view];
    self.title = @"OMZETTEN NAAR KRALEN";
    
    //    [_vwNextStep setupEmboss];
//    [_vwPearlify setupEmboss];
    [_vwImgeUser setupEmboss];
    NSData *imgData = [Common getImageDataFromDefaults:keyScaledImage];
    _imgUser.image = [UIImage imageWithData:imgData]?[UIImage imageWithData:imgData]:[UIImage imageNamed:@"girl.png"];
    vwSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    //[vwSpinner setCenter:CGPointMake(_imgUser.frame.size.width/2.0, _imgUser.frame.size.height/2.0)];
    
}
- (IBAction)btnPearlifyAction:(id)sender {
    if (checkInternetConnection()) {
        self.session.scaledImage = _imgUser.image;
        [self.session findWorkingServer:self];
//        hud = [Common showCustomHud:@"" label2:@"Please Wait"];
//        [hud show:YES];
//        [self.imgUser addSubview:hud];
        [vwSpinner startAnimating];
        [self.imgUser addSubview:vwSpinner];
        _vwPearlify.userInteractionEnabled = NO;
    }
    else {
        [Common showToastMessage:keyOfflineMessage withTitle:@"Connection Error" withAction:YES withMessageType:NO];
    }
    
    
//    [ApiManager sendImageForPearlification:imageData withSuccess:^(id result) {
//        
//    } withFailure:^(NSError *error) {
//        
//    }];
}

- (void)serverFound:(NSString *)serverName {
    if (serverName == nil) {
//        [hud removeFromSuperview];
        [vwSpinner stopAnimating];
        [vwSpinner removeFromSuperview];
        _vwPearlify.userInteractionEnabled = YES;
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Unable to reach server. Check your internet connection or try later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    } else {
        self.session.serverForCurrentImage = serverName;
        NSString* urlString = [NSString stringWithFormat:@"%@/AddImage", serverName];
        NSMutableURLRequest *request =
        [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        [request setTimeoutInterval:30.0];
        [request setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
        NSData* imageData = UIImagePNGRepresentation(self.session.scaledImage);
        [request setHTTPBody: imageData];
        self.apiManager.session = self.session;
        self.apiManager.currentSelection = getPearlifiedImage;
        [self.apiManager initWithRequest:request fromDelegate:self];
    }
    
}

-(void)gotPearlifiedImage:(UIImage *)imgPearlified andErrorMsg:(NSString *)error {
    
//    [hud removeFromSuperview];
    [vwSpinner stopAnimating];
    [vwSpinner removeFromSuperview];
    _vwPearlify.userInteractionEnabled = YES;
    if (!error) {
        _imgUser.image = imgPearlified;
       // NSString *strImage = getImageString(_imgUser.image);
        NSData *dataImage = [NSData dataWithData:UIImagePNGRepresentation(_imgUser.image)];
        [Common setImageDataToDefaults:keyPearledImage value:dataImage];
        _vwNextStep.alpha = 1.0;
        _vwNextStep.userInteractionEnabled = YES;
        _vwPearlify.alpha = 0.5;
        _vwPearlify.userInteractionEnabled = NO;
    }
   
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ResultViewController* resViewController = [segue destinationViewController];
    resViewController.session = self.session;
}
@end
