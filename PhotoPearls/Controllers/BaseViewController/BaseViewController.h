//
//  BaseViewController.h
//  TeamCommish
//
//  Created by Zeeshan Ahmed on 26/09/15.
//  Copyright © 2015 Teknowledge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Navbar.h"
#import "LocalizationSystem.h"
#import "Common.h"
#import "UIView+Addition.h"
#import "RectangleButton.h"
#import "ButtonView.h"
#import "ApiManager.h"
#import "PHPSession.h"
#import <MBProgressHUD/MBProgressHUD.h>

@class ApiManager;
@interface BaseViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *vwNavbar;
@property (strong,nonatomic) Navbar *navBar;
@property (nonatomic, retain) PHPSession* session;
@property (weak, nonatomic) IBOutlet UIView *mainContainer;
@property (nonatomic) ApiManager *apiManager;                //creating object for APImanager so that the instace methods can be accesed via this object
- (IBAction)backBtnAction:(id)sender;
- (void)serverFound:(NSString*)serverName;
-(void)addNavBarWithTitle:(NSString *)title andLeftButton:(NSString *)leftButton andRightButton:(NSString *)rightButton onView:(UIView*)onView ;
-(void)addNavBarWithTitle:(NSString *)title andLeftButton:(NSString *)leftButton andRightButton:(NSString *)rightButton onView:(UIView*)onView withSize:(float)size;
-(void)loadNavbar ;
-(void)receivedData:(id)data anderrorMsg:(NSString *)error ;
- (void)gotPearlifiedImage:(UIImage *)imgPearlified andErrorMsg:(NSString *)error ;
-(void)getAllFontNames ;
@end
