//
//  BaseViewController.m
//  TeamCommish
//
//  Created by Zeeshan Ahmed on 26/09/15.
//  Copyright © 2015 Teknowledge. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  

}

/*---------------------------------------------------------
 
 method to add the navbar to the view and adding the
 associated buttons
 
 
 but now this function is no more used as the UI of the app 
 changed and now it uses the system's navigation controller
 but this function initializes some objects such as the 
 apimanager so this method is called only for that
 ---------------------------------------------------------*/

-(void)addNavBarWithTitle:(NSString *)title andLeftButton:(NSString *)leftButton andRightButton:(NSString *)rightButton onView:(UIView*)onView {
//    NSShadow *shadow = [[NSShadow alloc] init];
//    shadow.shadowOffset = CGSizeMake(0.0, 1.0);
//    shadow.shadowColor = [UIColor blackColor];
//    
//    [[UINavigationBar appearance] setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      [UIColor blueColor], NSForegroundColorAttributeName,
//      shadow,NSShadowAttributeName,
//      [UIFont fontWithName:@"Arnold21" size:15.0], NSFontAttributeName,nil]];
//    
//    self.navigationController.navigationBar.translucent = YES;
    
    
//    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil]
//     setTitleTextAttributes:
//     @{NSForegroundColorAttributeName:[UIColor blackColor],
//       NSShadowAttributeName:shadow,
//       NSFontAttributeName:[UIFont fontWithName:@"Arnold21" size:12.0]
//       }
//     forState:UIControlStateNormal];
    
    [self initializeSessionObject];                 //for initializing PHPSession class which keeps track of the image settings and also finds servers from a pool of load balanced servers
    [self initializeApiObject];                     //initalized the apimanager object because using nsurlconnection and all of the methods are of instance type
    // [[UIBarButtonItem appearance] setTintColor: [UIColor brownColor]];
    
    if(!_navBar){
        [self loadNavbar];
        
        [_vwNavbar addSubview:_navBar];
        //[self.vwNavbar setShadowWithColor:[UIColor blackColor] withOpacity:0.5 withRadius:1.0 andOffset:2.0f];
        [self addFullResizeConstraints:_navBar addedOnParent:_vwNavbar];
    }
//    [self.navigationController setTitle:title];
//    [self.navigationController.navigationBar setTitleTextAttributes:
//     @{NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    self.navigationController.interactivePopGestureRecognizer.delegate=self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    NSLog(@"%@",NSStringFromCGRect(_vwNavbar.frame));
    NSLog(@"%@",NSStringFromCGRect(self.view.frame));
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    if ([ [ UIScreen mainScreen ] bounds ].size.height == 480) {
       // [self.navBar.lblTitle setFont:[UIFont systemFontOfSize:18]];
    }
    if ([ [ UIScreen mainScreen ] bounds ].size.width == 320) {
      //  [self.navBar.lblTitle setFont:[UIFont systemFontOfSize:15]];
    }
    _navBar.lblTitle.text=[NSString stringWithFormat:@"%@",AMLocalizedString(title, nil)];
     [_navBar.lblTitle setShadowWithColor:[UIColor blackColor] withOpacity:0.6 withRadius:0.4 andOffset:1.0];
    [_navBar.btnLeft addTarget:self action:@selector(pop) forControlEvents:UIControlEventTouchUpInside];
    
    if ([rightButton isEqualToString:@""]) {
        _navBar.btnRight.hidden = YES;
    }
    else {
        _navBar.btnRight.hidden = NO;
    }
    [_navBar.btnRight setTitle:rightButton forState:UIControlStateNormal];
    
}
-(void)addNavBarWithTitle:(NSString *)title andLeftButton:(NSString *)leftButton andRightButton:(NSString *)rightButton onView:(UIView*)onView withSize:(float)size{
    //    NSShadow *shadow = [[NSShadow alloc] init];
    //    shadow.shadowOffset = CGSizeMake(0.0, 1.0);
    //    shadow.shadowColor = [UIColor blackColor];
    //
    //    [[UINavigationBar appearance] setTitleTextAttributes:
    //     [NSDictionary dictionaryWithObjectsAndKeys:
    //      [UIColor blueColor], NSForegroundColorAttributeName,
    //      shadow,NSShadowAttributeName,
    //      [UIFont fontWithName:@"Arnold21" size:15.0], NSFontAttributeName,nil]];
    //
    //    self.navigationController.navigationBar.translucent = YES;
    
    
    //    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil]
    //     setTitleTextAttributes:
    //     @{NSForegroundColorAttributeName:[UIColor blackColor],
    //       NSShadowAttributeName:shadow,
    //       NSFontAttributeName:[UIFont fontWithName:@"Arnold21" size:12.0]
    //       }
    //     forState:UIControlStateNormal];
    
    [self initializeSessionObject];                 //for initializing PHPSession class which keeps track of the image settings and also finds servers from a pool of load balanced servers
    [self initializeApiObject];                     //initalized the apimanager object because using nsurlconnection and all of the methods are of instance type
    // [[UIBarButtonItem appearance] setTintColor: [UIColor brownColor]];
    
    if(!_navBar){
        [self loadNavbar];
        
        [_vwNavbar addSubview:_navBar];
        //[self.vwNavbar setShadowWithColor:[UIColor blackColor] withOpacity:0.5 withRadius:1.0 andOffset:2.0f];
        [self addFullResizeConstraints:_navBar addedOnParent:_vwNavbar];
    }
    //    [self.navigationController setTitle:title];
    //    [self.navigationController.navigationBar setTitleTextAttributes:
    //     @{NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    self.navigationController.interactivePopGestureRecognizer.delegate=self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    NSLog(@"%@",NSStringFromCGRect(_vwNavbar.frame));
    NSLog(@"%@",NSStringFromCGRect(self.view.frame));
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
  
      //  [self.navBar.lblTitle setFont:[UIFont systemFontOfSize:15]];
    
    _navBar.lblTitle.text=[NSString stringWithFormat:@"%@",AMLocalizedString(title, nil)];
    [_navBar.lblTitle setShadowWithColor:[UIColor blackColor] withOpacity:0.6 withRadius:0.4 andOffset:1.0];
    [_navBar.btnLeft addTarget:self action:@selector(pop) forControlEvents:UIControlEventTouchUpInside];
    
    if ([rightButton isEqualToString:@""]) {
        _navBar.btnRight.hidden = YES;
    }
    else {
        _navBar.btnRight.hidden = NO;
    }
    [_navBar.btnRight setTitle:rightButton forState:UIControlStateNormal];
    
}

-(void)loadNavbar {
    _navBar=[[[NSBundle mainBundle] loadNibNamed:@"Navbar" owner:self options:nil] lastObject];
}

-(void)initializeSessionObject {
    if (_session == nil) {
        self.session = [[PHPSession alloc]init];
    }
    
}
-(void)initializeApiObject {
    if (_apiManager == nil) {
        self.apiManager = [[ApiManager alloc]init];
    }
    
}
-(void)goToMenu{
//        MainMenuController *mainMenu = [self.storyboard instantiateViewControllerWithIdentifier:@"MainMenuController"];
//        [self.navigationController pushViewController:mainMenu animated:YES];
}

-(void)popToRoot{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

-(void)pop{
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//adds leading, trailing, top and bottom constraint to the parent view from the sub view

-(void)addFullResizeConstraints:(UIView *)subView addedOnParent:(UIView *)parent{
    
    
    
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   
                                   constraintWithItem:subView
                                   
                                   attribute:NSLayoutAttributeTrailing
                                   
                                   relatedBy:NSLayoutRelationEqual
                                   
                                   toItem:parent
                                   
                                   attribute:NSLayoutAttributeTrailing
                                   
                                   multiplier:1.0
                                   
                                   constant:0.f];
    
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 
                                 constraintWithItem:subView
                                 
                                 attribute:NSLayoutAttributeBottom
                                 
                                 relatedBy:NSLayoutRelationEqual
                                 
                                 toItem:parent
                                 
                                 attribute:NSLayoutAttributeBottom
                                 
                                 multiplier:1.0
                                 
                                 constant:0.f];
    
    NSLayoutConstraint *top = [NSLayoutConstraint
                               
                               constraintWithItem:subView
                               
                               attribute:NSLayoutAttributeTop
                               
                               relatedBy:NSLayoutRelationEqual
                               
                               toItem:parent
                               
                               attribute:NSLayoutAttributeTop
                               
                               multiplier:1.0
                               
                               constant:0.f];
    
    
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   
                                   constraintWithItem:subView
                                   
                                   attribute:NSLayoutAttributeLeading
                                   
                                   relatedBy:NSLayoutRelationEqual
                                   
                                   toItem:parent
                                   
                                   attribute:NSLayoutAttributeLeading
                                   
                                   multiplier:1.0
                                   
                                   constant:0.f];
    
    [parent addConstraint:trailing];
    
    [parent addConstraint:bottom];
    
    [parent addConstraint:top];
    
    [parent addConstraint:leading];
    
}

-(void)getAllFontNames {
//    for (NSString *fontFamilyName in [UIFont familyNames]) {
//        for (NSString *fontName in [UIFont fontNamesForFamilyName:fontFamilyName]) {
//            NSLog(@"Family: %@    Font: %@", fontFamilyName, fontName);
//        }
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//funtions which are to be implemented in the child classes
- (void)serverFound:(NSString*)serverName {
    
}
- (void)gotPearlifiedImage:(UIImage *)imgPearlified andErrorMsg:(NSString *)error {
    
}
-(void)receivedData:(id)data anderrorMsg:(NSString *)error {
    
}


@end
