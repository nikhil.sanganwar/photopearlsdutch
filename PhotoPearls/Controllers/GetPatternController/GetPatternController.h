//
//  GetPatternController.h
//  PhotoPearls
//
//  Created by Priyesh Das on 11/2/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface GetPatternController : BaseViewController<UITextFieldDelegate,ABPeoplePickerNavigationControllerDelegate>

@end
