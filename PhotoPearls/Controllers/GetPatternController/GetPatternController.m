//
//  GetPatternController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 11/2/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "GetPatternController.h"

@interface GetPatternController ()
@property (weak, nonatomic) IBOutlet ButtonView *vwAddressBook;
@property (weak, nonatomic) IBOutlet ButtonView *vwSendPattern;
@property (weak, nonatomic) IBOutlet UIView *vwImgUser;
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UIButton *btnSendPattrn;

@end

@implementation GetPatternController
{
    UIGestureRecognizer *tap;
    MBProgressHUD *hud;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupView {
    
    if ([Common isiPad]) {
        //self.font.fontName
        self.txtFldEmail.font = [UIFont fontWithName:@"Helvetica-Bold" size:22];
    }else {
        self.txtFldEmail.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    }
    _txtFldEmail.delegate = self;
    UIColor *color = [UIColor lightGrayColor];
    _txtFldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Vul e-mailadres in"
                                                attributes:@{
                                                                NSForegroundColorAttributeName: [UIColor blackColor],
                                                                NSFontAttributeName : [UIFont systemFontOfSize:17]
                                                            }];
    
    [_vwImgUser setCornerRadius:5.0];
    //[self addNavBarWithTitle:@"CONSIGUE PATRÓN DE ABALORIO" andLeftButton:@"back" andRightButton:@"" onView:self.view];
     //bynikhil4417
    [self addNavBarWithTitle:@"PATROON" andLeftButton:@"back" andRightButton:@"" onView:self.view];

    self.title = @"GÉNÉRER LE PATRON";
    NSData *imgData = [Common getImageDataFromDefaults:keyPearledImage];
    _imgUser.image = [UIImage imageWithData:imgData]?[UIImage imageWithData:imgData]:[UIImage imageNamed:@"girl.png"];
//    _imgUser.image = [Common getUserDefaults:keyPearledImage]?getImageFromString([Common getUserDefaults:keyPearledImage]):[UIImage imageNamed:@"girl.jpg"];
//    [_vwAddressBook setupEmboss];
//    [_vwSendPattern setupEmboss];
    [self ButtonSendEnable:NO];
    
}

-(void)ButtonSendEnable:(BOOL)enable {
    if (enable == NO) {
        _btnSendPattrn.userInteractionEnabled = NO;
        _vwSendPattern.alpha = 0.5;
    }
    else {
        _btnSendPattrn.userInteractionEnabled = YES;
        _vwSendPattern.alpha = 1.0;
    }
    
}
- (IBAction)btnSendPatternAction:(id)sender {
    if (isEmailValid(_txtFldEmail.text)) {
        
        if (checkInternetConnection()) {
            NSString* urlString = [NSString stringWithFormat:@"%@/SendPearlPatternPrintout/imageId:%@/recipient:%@", self.session.serverForCurrentImage, self.session.imageId, _txtFldEmail.text];
            NSMutableURLRequest	*request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"GET"];
            self.apiManager.currentSelection = sendPattern;
            [self.apiManager initWithRequest:request fromDelegate:self];
            hud = [Common showCustomHud:@"" label2:@"Please Wait"];
            [hud show:YES];
            [self.view addSubview:hud];
        }
        else {
            
            [Common showToastMessage:keyOfflineMessage withTitle:@"Connection Error" withAction:YES withMessageType:NO];
        }
        
    }
    else {
        [Common showToastMessage:@"Please provide a valid email" withTitle:@"Sorry" withAction:YES withMessageType:NO];
    }
    
   
    
}
- (IBAction)btnGetAddressAction:(id)sender {
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    // place the delegate of the picker to the controll
    picker.displayedProperties = [NSArray arrayWithObjects:
                                            [NSNumber numberWithInt:kABPersonEmailProperty],
                                            nil];

    picker.peoplePickerDelegate = self;
    
    [self.navigationController presentViewController:picker animated:YES completion:^{
        
    }];
}

//this is a derived method to receive data from apimanager,the master class is declared in the baseviewcontroller
-(void)receivedData:(NSURLResponse *)data anderrorMsg:(NSString *)error {
    [hud removeFromSuperview];
    NSHTTPURLResponse* myResponse = (NSHTTPURLResponse*)data;
    NSString *titleString;
    NSString *msgString;
    switch (myResponse.statusCode) {
        case 200:
            titleString = @"Pattern sent!";
            msgString = [NSString stringWithFormat:@"The bead pattern for your image was sent to %@. Look for it there!", _txtFldEmail.text];
            break;
        case 500:
            titleString = @"Failed to Send Pattern";
            msgString   = @"Your pattern was not sent. Check the e-mail address and try again.";
            break;
        default:
            titleString = @"Failed to Send Pattern";
            msgString   = @"Your pattern was not sent. Cause unknown.";
            break;
    }
    
    UIAlertView* alert = [[UIAlertView alloc]
                          initWithTitle:titleString
                          message: msgString
                          delegate:nil cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}


#pragma  mark
#pragma mark Text Field Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];

}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    [self ButtonSendEnable:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self dismissKeyboard];
    return YES;
}

-(void)dismissKeyboard{
    
    [_txtFldEmail resignFirstResponder];
    [self.view removeGestureRecognizer:tap];
    tap=nil;
    
}


#pragma mark
#pragma mark AbPeoplePicker delegates
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    // assigning control back to the main controller
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (BOOL)peoplePickerNavigationController: (ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    /*
     // setting the first name
     firstName.text = (NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
     
     // setting the last name
     lastName.text = (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
     
     // setting the number
     
     this function will set the first number it finds
     
     if you do not set a number for a contact it will probably
     crash
     
     ABMultiValueRef multi = ABRecordCopyValue(person, kABPerson);
     number.text = (NSString*)ABMultiValueCopyValueAtIndex(multi, 0);
     
     // remove the controller
     [self dismissModalViewControllerAnimated:YES];
     */
    return YES;
}
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    
    if (property == kABPersonEmailProperty) {
        ABMultiValueRef multiValue = ABRecordCopyValue(person, kABPersonEmailProperty);
        CFIndex index = ABMultiValueGetIndexForIdentifier(multiValue, identifier);
        _txtFldEmail.text = (__bridge NSString *)ABMultiValueCopyValueAtIndex(multiValue, index);
        
        [self.parentViewController dismissViewControllerAnimated:YES completion:^{
            
        }];
        [self ButtonSendEnable:YES];
        return NO;
        
    } else {
        return NO;
    }
    
}

@end
