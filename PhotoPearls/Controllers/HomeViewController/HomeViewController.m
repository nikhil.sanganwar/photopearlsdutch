//
//  HomeViewController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 10/30/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "HomeViewController.h"
#import "ChooseImageController.h"
#import "PearlifyViewController.h"
#import "WebElementsDisplayController.h"

@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet ButtonView *vwCreatePattern;
@property (weak, nonatomic) IBOutlet ButtonView *vwStoreFinder;
@property (weak, nonatomic) IBOutlet ButtonView *vwAboutPhotoPearls;
@property (weak, nonatomic) IBOutlet UIView *vwBtnContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopLogo;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (IS_IPHONE_4_OR_LESS) {
        _constraintTopLogo.constant = 0;
    }
    
    [self getAllFontNames];
    [self setupView];
}
-(void)viewDidAppear:(BOOL)animated {
   
    
    [Common deleteUserDefaults:keyPearledImage];
    [Common deleteUserDefaults:keyScaledImage];
//    NSString *strImage = getImageString([UIImage imageNamed:@"girl.jpg"]);
    NSData *dataImage = [NSData dataWithData:UIImagePNGRepresentation([UIImage imageNamed:@"girl.jpg"])];
    [Common setImageDataToDefaults:keyUserImage value:dataImage];
    self.navigationController.interactivePopGestureRecognizer.delegate=self;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}
-(void)viewWillAppear:(BOOL)animated {
     [self.navigationController.navigationBar setHidden:YES];
}

-(void)viewDidDisappear:(BOOL)animated {
     [self.navigationController.navigationBar setHidden:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupView {
    [self loadNavbar];
    self.title = @"menu principal";
    DLog(@"%@",NSStringFromCGRect(_vwCreatePattern.frame));
     DLog(@"%@",NSStringFromCGRect(_vwBtnContainer.frame));
   // [[UINavigationBar appearance] setTintColor:[UIColor brownColor]];
//    float degrees =  -2; //the value in degrees
//    [_vwCreatePattern setTransform:CGAffineTransformMakeRotation(degrees * M_PI/180)];
//    float degree =  2; //the value in degrees
//    [_vwAboutPhotoPearls setTransform:CGAffineTransformMakeRotation(degree * M_PI/180)];
//    
//    [_vwCreatePattern setShadowWithColor:[UIColor blackColor] withOpacity:0.8 withRadius:2.0 andOffset:4.0];
//    [_vwAboutPhotoPearls setShadowWithColor:[UIColor blackColor] withOpacity:0.8 withRadius:2.0 andOffset:4.0];
//    [_vwStoreFinder setShadowWithColor:[UIColor blackColor] withOpacity:0.8 withRadius:2.0 andOffset:4.0];
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    if (self.session == nil) {
        self.session = [[PHPSession alloc] init];
    }
    [self.session findWorkingServer:self];
}
- (IBAction)btnCreatePatternAction:(id)sender {
    
    ChooseImageController *chooseImage = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ChooseImageController class])];
    [self.navigationController pushViewController:chooseImage animated:YES];
}

- (void)serverFound:(NSString *)serverName {
    if (serverName == nil) {
//        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Unable to reach server. Check your internet connection or try later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
    }
    else {
        NSLocale* locale = [NSLocale currentLocale];
        NSString* localeID = [locale objectForKey:NSLocaleCountryCode];
        
        NSString* urlString = [NSString stringWithFormat:@"%@/GetPartnerLogoByCountry/country:%@", serverName, localeID];
        NSMutableURLRequest	*request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"GET"];
        
        //initialize the apimanager class, only needed in this view controller because the rest of the classes has it already initialized in the load navbar method in baseview controller,but this controller has doesn't call the load navbar function, hence it is needed
         self.apiManager = [[ApiManager alloc]init];
        self.apiManager.session = self.session;
        self.apiManager .currentSelection = defaultImageReq;
        [self.apiManager initWithRequest:request fromDelegate:self];
    }
    
}

-(void)receivedData:(id)data anderrorMsg:(NSString *)error {
    
    if (!error) {
        UIImage* imageObj = [[UIImage alloc] initWithData:data];
        NSString *strImage = getImageString(imageObj);
        if (!isEmpty(strImage)) {
            [Common setImageDataToDefaults:keyUserImage value:data];
        }
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"onlinestore"])
    {
        // Get reference to the destination view controller
        WebElementsDisplayController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        vc.isStoreLocatorDisplay = YES;
    }
    else {
        // Get reference to the destination view controller
        WebElementsDisplayController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        vc.isStoreLocatorDisplay = NO;
    }
}

@end
