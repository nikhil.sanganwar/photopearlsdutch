//
//  ResultViewController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 10/30/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "ResultViewController.h"
#import "GetPatternController.h"
#import "ShoppingListController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "WebElementsDisplayController.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"

@interface ResultViewController ()
@property (weak, nonatomic) IBOutlet ButtonView *vwImgUser;
@property (weak, nonatomic) IBOutlet ButtonView *vwGetPattern;
@property (weak, nonatomic) IBOutlet ButtonView *vwShoppingList;
@property (weak, nonatomic) IBOutlet ButtonView *vwStoreFinder;
@property (weak, nonatomic) IBOutlet ButtonView *vwFBShare;
@property (weak, nonatomic) IBOutlet ButtonView *vwSaveToPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong,nonatomic) ALAssetsLibrary *library;

@end
typedef enum
{
    btnBeadPattern = 1,
    btnShoppingList,
    btnStoreFinder,
    btnFBShare,             //this button is currently removed but the sender and the tag and the functionality is kept for future reference or if in future it is decided to be used again.
    btnSavePhoto
} getBtnType;

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupView {
    [_vwImgUser setCornerRadius:5.0];
    [_vwImgUser setupEmboss];
//    [self addNavBarWithTitle:@"RESULTADOS" andLeftButton:@"back" andRightButton:@"4" onView:self.view];
//    self.title = @"RESULTATS";
     //bynikhil4417
    [self addNavBarWithTitle:@"RESULTAAT" andLeftButton:@"back" andRightButton:@"4" onView:self.view];
    self.title = @"RESULTAAT";
    NSData *imgData = [Common getImageDataFromDefaults:keyPearledImage];
    _imgUser.image = [UIImage imageWithData:imgData]?[UIImage imageWithData:imgData]:[UIImage imageNamed:@"girl.png"];
//     _imgUser.image = [Common getUserDefaults:keyPearledImage]?getImageFromString([Common getUserDefaults:keyPearledImage]):[UIImage imageNamed:@"girl.jpg"];
//    [_vwStoreFinder setupEmboss];
//    [_vwShoppingList setupEmboss];
//    [_vwSaveToPhoto setupEmboss];
//    [_vwGetPattern setupEmboss];
//    [_vwFBShare setupEmboss];
    self.library = [[ALAssetsLibrary alloc]init];
}

- (IBAction)btnClickAction:(UIButton *)sender {
    
    switch (sender.tag) {
        case btnBeadPattern:
            
            break;
        case btnStoreFinder:
        {
            
        }
            break;
        case btnShoppingList: {
            [self goToShoppingList];
        }
            break;
        case btnFBShare: {
//            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//            content.contentURL = [NSURL URLWithString:imgUrl];
//            content.imageURL = [NSURL URLWithString:imgUrl];
//            content.contentTitle = AMLocalizedString(@"%@",[dictCouponDetails valueForKey:@"name"]);
//            content.contentDescription = AMLocalizedString(@"Hey I am using it", nil);
//            [FBSDKShareDialog showFromViewController:self
//                                         withContent:content
//                                            delegate:self];
            
            
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = _imgUser.image;
            //photo.userGenerated = YES;
//            photo.caption = @"My Awesome Photo using Photo Pearls";
            FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
            content.photos = @[photo];
            FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:nil];
            
            // Create an action
            FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
            action.actionType = @"books.reads";
            [action setObject:object forKey:@"books:book"];
            
            // Add the photo to the action. Actions
            // can take an array of images.
            [action setArray:@[photo] forKey:@"image"];
            
            // Create the content
            FBSDKShareOpenGraphContent *content1 = [[FBSDKShareOpenGraphContent alloc] init];
            content1.action = action;
            content1.previewPropertyName = @"books:book";
            
            [FBSDKShareDialog showFromViewController:self
                                         withContent:content1
                                            delegate:nil];
            
            
//            FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
//            dialog.fromViewController = self;
//            dialog.shareContent = content;
//            dialog.mode = FBSDKShareDialogModeBrowser;
//            [dialog show];
            
            
//            [FBSDKShareDialog showFromViewController:self
//                                         withContent:content
//                                            delegate:self];
        }
            break;
        case btnSavePhoto: {
            [self.library saveImage:_imgUser.image toAlbum:@"Photo Pearls" withCompletionBlock:^(NSError *error) {
                if (error!=nil) {
                    NSLog(@"Big error: %@", [error description]);
                }
                else {
//                    [Common showToastMessage:@"De foto is opgeslagen in je fotobiblitoheek. Bekijk het daar! " withTitle:@"Foto opgeslagen" withAction:NO withMessageType:NO];
                    [self addNavBarWithTitle:@"Opslaan" andLeftButton:@"back" andRightButton:@"4" onView:self.view];

                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Foto opgeslagen" message:@"De foto is opgeslagen in je fotobiblitoheek. Bekijk het daar! " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                    
                }
            }];
            
        }
            break;
        default:
            break;
    }
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if (buttonIndex == 0)
    {
        [self addNavBarWithTitle:@"RESULTAAT" andLeftButton:@"back" andRightButton:@"4" onView:self.view];

        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"OK Tapped. Hello World!");
    }
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"onlinestore"])
    {
        // Get reference to the destination view controller
        WebElementsDisplayController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        vc.isStoreLocatorDisplay = YES;
    }
    else if ([[segue identifier] isEqualToString:@"beadpattern"]) {
        GetPatternController* pattViewController = [segue destinationViewController];
        pattViewController.session = self.session;
    }
    
    
}

-(void)goToShoppingList {
    ShoppingListController *shoppingList = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ShoppingListController class])];
    shoppingList.session = self.session;
    [self.navigationController pushViewController:shoppingList animated:YES];

}

#pragma mark
#pragma mark Share methods

-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
//    if ([results valueForKey:@"postId"]) {
        [Common showToastMessage:@"Successfully shared" withTitle:@"" withAction:YES withMessageType:NO];
//    }
    
}
-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {

    [Common showToastMessage:@"experiencing some problems,please try later" withTitle:@"" withAction:YES withMessageType:NO];
}

-(void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    //[Common showToastMessage:@"Successfully shared" withTitle:@"" withAction:YES withMessageType:NO];
}


@end
