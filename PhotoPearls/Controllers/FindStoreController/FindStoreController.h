//
//  FindStoreController.h
//  PhotoPearls
//
//  Created by Priyesh Das on 11/4/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BaseViewController.h"

@interface FindStoreController : BaseViewController<CLLocationManagerDelegate>

@end
