//
//  FindStoreController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 11/4/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "FindStoreController.h"
#import "WebshopViewCell.h"
#import <GoogleMaps/GoogleMaps.h>
#import "WebElementsDisplayController.h"
#import <MapKit/MapKit.h>

@interface FindStoreController ()
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *vwGoogleMap;           //view on which the map will be displayed
@property (strong,nonatomic) GMSMapView *mapView_;
@property (strong,nonatomic) MKMapView *mapView2_;                  //earlier the google map was being used but now it changed to mkmap,hence adding another and not changing the full layout
@property (strong,nonatomic) NSMutableArray *arrShops;
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) NSString *myServer;
@property (strong,nonatomic) NSArray *webShopArray;
@property BOOL isMapOn;
@end

@implementation FindStoreController
{
    BOOL locationFound;
    int ZoomMeter;
    CLLocation *myLocation;
    CLLocationDegrees strLatitude;
    CLLocationDegrees strLongitude;
}
@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addNavBarWithTitle:@"Trouver un magasin" andLeftButton:@"back" andRightButton:@"" onView:self.view];
    self.title = @"Trouver un magasin";
    _tblView.hidden = YES;
    ZoomMeter = 0;
    if (checkInternetConnection()) {
        locationManager = [[CLLocationManager alloc]init];
        //update the location manager to fetch current location
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];
    }
    else {
        [Common showToastMessage:keyOfflineMessage withTitle:@"Connection Error" withAction:YES withMessageType:NO];
    }

    
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    [self setupView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupView {
    
    //set the table view header
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblView.frame.size.width, 30)];
    [headerView setBackgroundColor:[UIColor lightGrayColor]];
    UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, headerView.frame.size.width - 10, headerView.frame.size.height)];
    [headerView addSubview:labelView];
    labelView.textAlignment = NSTextAlignmentLeft;
    labelView.text = @"Available Webshops";
    self.tblView.tableHeaderView = headerView;
    
    NSArray *arrData = @[@{@"name":@"Justin",@"selected":@"0",@"image":@"user1.png",@"player-no":@"12",@"gameType":@"basketball"},
                         @{@"name":@"Kristina",@"selected":@"0",@"image":@"user2.png",@"player-no":@"12",@"gameType":@"soccer"},
                         @{@"name":@"Judy",@"selected":@"0",@"image":@"user3.png",@"player-no":@"12",@"gameType":@"tennis"},
                         @{@"name":@"Aki Anis",@"selected":@"0",@"image":@"user4.png",@"player-no":@"12",@"gameType":@"basketball"}];
    
    
    _arrShops = [[NSMutableArray alloc]initWithArray:arrData];
    
//    //fetch webshops in background
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,2.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [self fetchWebshops];
//    });
//    
}

- (void)serverFound:(NSString *)serverName {
    if (serverName == nil) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"No Connection" message:@"Unable to to connect to server." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    } else {
        self.myServer = serverName;
        [self findStoresAutomatically];
    }
    
}

-(void)fetchWebshops {
    
                NSString *keyUserName           =   @"munkplast-iphone";
                NSString *keyPassword           =   @"yu!sux!Q";
                NSLocale* locale = [NSLocale currentLocale];
                NSString* localeID = [locale objectForKey:NSLocaleCountryCode];
                NSString *authStr = [NSString stringWithFormat:@"%@:%@",keyUserName,[PHPSession passWd]];
                //NSString *authStr = @"user1:pass";
				NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
				NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
				
				
				NSString* urlString = [NSString stringWithFormat:@"%@/GetPartnerWebshopsByCountry/country:%@", [self.session server], localeID];
				NSMutableURLRequest *request =
				[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
				[request setHTTPMethod:@"GET"];
				//[request setValue:authValue forHTTPHeaderField:@"Authorization"];
//                self.apiManager.currentSelection = getWebshops;
//                [self.apiManager initWithRequest:request fromDelegate:self];
                NSError *jsonError;
				NSData* returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
                NSString* storeString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                NSData *objectData = [storeString dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary* storeDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                              options:NSJSONReadingMutableContainers
                                                                error:&jsonError];
				_webShopArray = [storeDict objectForKey:@"webshops"];
}
-(void)setGoogleMap{
    
    //set the google maps mapview
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:strLatitude
                                                            longitude:strLongitude
                                                                 zoom:2];
    _mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, _vwGoogleMap.frame.size.width, _vwGoogleMap.frame.size.height) camera:camera];
    _mapView_.myLocationEnabled = YES;
    [self.vwGoogleMap addSubview:_mapView_];
    camera = [GMSCameraPosition cameraWithLatitude:strLatitude
                                         longitude:strLongitude
                                              zoom:12];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat: 2.0f] forKey:kCATransactionAnimationDuration];
        // change the camera, set the zoom, whatever.  Just make sure to call the animate* method.
        [_mapView_ animateToCameraPosition: camera];
        [CATransaction commit];
        [self.session findWorkingServer:self];
    });
    
    
}

-(void)setMKMap {
    
    _mapView2_ = [[MKMapView alloc]initWithFrame:CGRectMake(0, 30, _vwGoogleMap.frame.size.width, _vwGoogleMap.frame.size.height - 80)];
    [_vwGoogleMap addSubview:_mapView2_];
    _mapView2_.showsUserLocation = YES;
}

#pragma mark -
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil && !locationFound) {
        locationFound = YES;
        myLocation = currentLocation;
        strLongitude =   currentLocation.coordinate.longitude;
        strLatitude =    currentLocation.coordinate.latitude;
        [self setMKMap];
        [locationManager stopUpdatingLocation];
    }
    
}

#pragma mark -
#pragma mark Table View Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  [_webShopArray count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"WebshopViewCell";
    
    WebshopViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    NSDictionary* shop = [_webShopArray objectAtIndex:indexPath.row];
    NSString* shopName = [shop objectForKey:@"name"];
    NSString* shopURL = [shop objectForKey:@"url"];
    cell.textLabel.text = shopName;
    cell.detailTextLabel.text = shopURL;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WebElementsDisplayController* nextController = [[WebElementsDisplayController alloc] initWithNibName:@"WebElementsDisplayController" bundle:nil];
    nextController.session = self.session;
    nextController.isStoreLocatorDisplay = NO;
    NSDictionary* shop = [_webShopArray objectAtIndex:indexPath.row];
    NSString* shopURL = [shop objectForKey:@"url"];
    nextController.urlAddress = shopURL;
    
    [self.navigationController pushViewController:nextController animated:YES];
}

#pragma mark
#pragma mark button actions
- (IBAction)btnSelectionAction:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        [self addNavBarWithTitle:@"Find Store" andLeftButton:@"back" andRightButton:@"" onView:self.view];
        self.title = @"Find Stores";
        _tblView.hidden = YES;
        _mapView2_.hidden = NO;
    }
    else{
        [self addNavBarWithTitle:@"Find Webshops" andLeftButton:@"back" andRightButton:@"" onView:self.view];
        self.title = @"Find Webshops";
        _tblView.hidden = NO;
        _mapView2_.hidden = YES;
        [self fetchWebshops];
    }
    
}
- (IBAction)btnRefreshAction:(id)sender {
     [self.session findWorkingServer:self];
}

#pragma  mark -
#pragma mark Fetch stores

-(void)findStoresAutomatically {
    [self retrieveStoreData:10 atLocation:myLocation within:20000];
}

- (void)retrieveStoreData:(int)numberOfStores atLocation:(CLLocation*)location within:(int)range {
    
    //- (void)retrieveStoreData {
    NSLocale* locale = [NSLocale currentLocale];
    NSString* localeID = [locale objectForKey:NSLocaleCountryCode];
    //NSString* localeID = @"UK";
    /*
     NSString* urlString = [NSString stringWithFormat: @"%@/GetPartnerStoresByCountry/country:%@/latitude:%f/longitude:%f",
     @"Blö", //self.myServer,
     localeID,
     location.coordinate.latitude,
     location.coordinate.longitude];
     //numberOfStores,
     //range];*/
    myLocation = location;
    //	CLLocationDegrees lat = location.coordinate.latitude;
    //	CLLocationDegrees lon = location.coordinate.longitude;
    NSString* urlString = [NSString stringWithFormat:@"https://goliath.services/nl/",//@"%@/GetPartnerStoresByCountry/country:%@/latitude:%f/longitude:%f",
                           _myServer,
                           localeID,
                           strLatitude,
                           strLongitude
                           ];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    self.apiManager.currentSelection = getShopsConnection;
    [self.apiManager initWithRequest:request fromDelegate:self];
}

-(void)receivedData:(NSArray *)data anderrorMsg:(NSString *)error{
    
    if (self.apiManager.currentSelection == getShopsConnection) {
        if (data.count == 0) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"No Stores Found" message:@"There are no registered stores near you that sell Photopearls products. We suggest that you shop online." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else {
            for (int i = 0; i <data.count; i++) {
                NSDictionary* dict = (NSDictionary*)[data objectAtIndex:i];
                CLLocation* store = [[CLLocation alloc] init];
                float latitude  = [(NSNumber*)[dict objectForKey:@"latitude"] floatValue];
                float longitude  = [(NSNumber*)[dict objectForKey:@"longitude"] floatValue];
                
                NSString *name = isEmpty([dict objectForKey:@"name"])?@"":[dict objectForKey:@"name"];
//                GMSMarker *currLocMarker = [[GMSMarker alloc] init];
//                [currLocMarker setTitle:AMLocalizedString(name, nil)];
//                currLocMarker.position = CLLocationCoordinate2DMake(latitude, longitude);
//                currLocMarker.map = self.mapView_;
                //        store.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
                //        store.name = (NSString*)[dict objectForKey:@"name"];
                //        store.streetAddress = (NSString*)[dict objectForKey:@"streetAddress"];
                //        [_mapView_ addAnnotation:store];
                //        [mapAnnotations addObject:store];
                
                
                // Add the annotation to our map view
                MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
                [annotation setCoordinate:CLLocationCoordinate2DMake(latitude, longitude)];
                [annotation setTitle:name]; //You can set the subtitle too
                [self.mapView2_ addAnnotation:annotation];
            }
            
        }
        [self fetchWebshops];
    }
    
    
}

@end
