//
//  ChooseImageController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 10/30/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "ChooseImageController.h"
#import "UIImage+Resize.h"

@interface ChooseImageController ()
@property (weak, nonatomic) IBOutlet ButtonView *vwCamera;
@property (weak, nonatomic) IBOutlet ButtonView *vwSelectExisting;
@property (weak, nonatomic) IBOutlet ButtonView *vwNext;
@property (weak, nonatomic) IBOutlet ButtonView *vwImageUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthViewImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewImage;
- (IBAction)btnSelectImageAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UIView *vwBtnContainer;

@end

@implementation ChooseImageController
{
    MBProgressHUD *hud;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self addNavBarWithTitle:@"ELIGE UNA IMAGEN" andLeftButton:@"back" andRightButton:@"1" onView:self.view];
     //bynikhil4417
    [self addNavBarWithTitle:@"KIES FOTO" andLeftButton:@"back" andRightButton:@"1" onView:self.view];

    self.title = @"CHOISISSEZ IMAGE";
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

// This particular snippet of code is used so that the status bar doesn't re appear after selecting image from uiimagepicker
// UIImagepickercontroller is a type of navigation controller hence when we set pickercontroller's delegate to self then we are setting this controller as the UIInavigationcontroller's delegate also
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}
-(void)setupView {
    [_vwImageUser setCornerRadius:5.0];
    [_vwImageUser setupEmboss];
    DLog(@"%@",NSStringFromCGRect(_vwCamera.frame));
    DLog(@"%@",NSStringFromCGRect(_vwBtnContainer.frame));
//    [_vwCamera setupEmboss];
//    [_vwSelectExisting setupEmboss];
//    [_vwNext setupEmboss];
     //_constraintWidthViewImage.constant = _vwImageUser.frame.size.height;
   
}
- (IBAction)btnSelectImageAction:(UIButton *)sender {
    
    if (sender.tag == 1) {
        [self showImagePickerwithCamera:YES];
    }
    else {
        [self showImagePickerwithCamera:NO];
    }
}

-(void)showImagePickerwithCamera:(BOOL)withCamera {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    //picker.allowsEditing = YES;
    picker.sourceType =(withCamera && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) ?UIImagePickerControllerSourceTypeCamera:UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark -
#pragma - mark Selecting Image from Camera and Library
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage;
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0") && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        chosenImage = info[UIImagePickerControllerOriginalImage];\
//    }
//    else {
//        chosenImage = info[UIImagePickerControllerOriginalImage];
//    }
    chosenImage = info[UIImagePickerControllerOriginalImage];
    chosenImage = [chosenImage resizedImageForProperOrientation:chosenImage.size interpolationQuality:0.6];
//
    _imgUser.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    _vwNext.userInteractionEnabled = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
         //NSString *strImage = getImageString(_imgUser.image);
         NSData *dataImage = [NSData dataWithData:UIImagePNGRepresentation(_imgUser.image)];
        [Common setImageDataToDefaults:keyUserImage value:dataImage];
        _vwNext.userInteractionEnabled = YES;
    });
    
    
    
}


@end
