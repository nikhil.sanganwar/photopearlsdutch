//
//  ScaleImageController.h
//  PhotoPearls
//
//  Created by Priyesh Das on 10/30/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <RSKImageCropper/RSKImageCropper.h>

@interface ScaleImageController : BaseViewController<UIScrollViewDelegate,RSKImageCropViewControllerDelegate>

@end
