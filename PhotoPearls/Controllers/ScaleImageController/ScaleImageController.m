//
//  ScaleImageController.m
//  PhotoPearls
//
//  Created by Priyesh Das on 10/30/15.
//  Copyright © 2015 Teknowledge Software. All rights reserved.
//

#import "ScaleImageController.h"
#import "UIImage+Resize.h"
#import "UIImage+Scale.h"
#import "UtilityView.h"
#import "PearlifyViewController.h"

@interface ScaleImageController ()
@property (weak, nonatomic) IBOutlet ButtonView *vwNextStep;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblBeadCount;
@property (weak, nonatomic) IBOutlet UIButton *btnScale1;
@property (weak, nonatomic) IBOutlet UIButton *btnScale2;
@property (weak, nonatomic) IBOutlet UIButton *btnScale3;
@property (weak, nonatomic) IBOutlet UIButton *btnScale4;
@property (weak, nonatomic) IBOutlet UIView *vwImageUser;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVwImgUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEqWidthImgUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEqHeightImgUser;
@property (nonatomic) CGRect ScrollviewFrame;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeadingButton1;
@end

@implementation ScaleImageController
{
    UtilityView *utilityView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addNavBarWithTitle:@"POSITIONEREN" andLeftButton:@"back" andRightButton:@"2" onView:self.view];
    self.title = @"ECHELLE";
    if (IS_IPHONE_4_OR_LESS) {
        _constraintLeadingButton1.constant = 0;
    }
    else {
        _constraintLeadingButton1.constant = 10;
    }
//    [_vwNextStep setupEmboss];
    
//    CGRect frame = _scrollVwImgUser.frame;
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
//        NSLayoutConstraint *width =[NSLayoutConstraint
//                                    constraintWithItem:_imgUser
//                                    attribute:NSLayoutAttributeWidth
//                                    relatedBy:NSLayoutRelationEqual
//                                    toItem:_scrollVwImgUser
//                                    attribute:NSLayoutAttributeWidth
//                                    multiplier:1.1
//                                    constant:0];
//        NSLayoutConstraint *height =[NSLayoutConstraint
//                                     constraintWithItem:_imgUser
//                                     attribute:NSLayoutAttributeHeight
//                                     relatedBy:NSLayoutRelationEqual
//                                     toItem:_scrollVwImgUser
//                                     attribute:NSLayoutAttributeHeight
//                                     multiplier:1.1
//                                     constant:0];
//        [_scrollVwImgUser addConstraint:width];
//        [_scrollVwImgUser addConstraint:height];
//        CGRect frame = _scrollVwImgUser.frame;
//        CGRect frame2 = _imgUser.frame;
//    }
//    _imgUser.image = [UIImage imageNamed:@"girl.jpg"];
}

//method to calculate the frame of the imageview to fit in the given view,the shorter end is taken and then it is made equal to one end.i.e if the width is hsowrter then the width of the imgvw is made that of the width of the vwImgUser,and the height is then adjusted accordingly.This is done like this because this is how the functionality is required.
-(CGRect )getImgVwFrameFromImg:(UIImage *)img {
    CGRect frame;
    CGFloat divRatio;
    frame.size = img.size;
    int height = frame.size.height;
    int width = frame.size.width;
    
    if (frame.size.height > frame.size.width) {
        divRatio = width/_vwImageUser.frame.size.width;
        if (divRatio != 0) {
            height = height/divRatio;
            width = width/divRatio;
        }
        else {
            height = _vwImageUser.frame.size.height;
            width =_vwImageUser.frame.size.width;
        }
            
        
    }
    else if (frame.size.height < frame.size.width) {
        
        divRatio = height/_vwImageUser.frame.size.height;
        if (divRatio != 0) {
            height = height/divRatio;
            width = width/divRatio;
        }
        else {
            height = _vwImageUser.frame.size.height;
            width =_vwImageUser.frame.size.width;
        }
        
    }
    else {
         divRatio = height/_vwImageUser.frame.size.height;
        height = height/divRatio;
        width = width/divRatio;
    }
    
    frame.origin.x=0;
    frame.origin.y = 0;
    frame.size.width = width;
    frame.size.height = height;
    
    return frame;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
    
}

-(void)viewDidAppear:(BOOL)animated {
    [self setupView];
}
-(void)viewWillDisappear:(BOOL)animated {
    [utilityView removeFromSuperview];
}
-(void)setupView{
    
    [_imgUser removeFromSuperview];
    NSData *imgData = [Common getImageDataFromDefaults:keyUserImage];
    UIImage *img = [UIImage imageWithData:imgData]?[UIImage imageWithData:imgData]:[UIImage imageNamed:@"girl.png"];
    if (img.size.width > _vwImageUser.frame.size.width || img.size.height > _vwImageUser.frame.size.height) {
        _imgUser = [[UIImageView alloc]initWithFrame:[self getImgVwFrameFromImg:img]];
    }
    else {
        _imgUser = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _scrollVwImgUser.frame.size.width, _scrollVwImgUser.frame.size.height)];
    }
    
    _imgUser.translatesAutoresizingMaskIntoConstraints = YES;
    [_imgUser setContentMode:UIViewContentModeScaleAspectFill];
//    [_imgUser setBackgroundColor:[UIColor blackColor]];
     [_imgUser setImage:img];
    
    //_imgUser.image = [UIImage imageWithData:imgData]?[UIImage imageWithData:imgData]:[UIImage imageNamed:@"girl.png"];
    CGRect imgFrame;
    imgFrame = _imgUser.frame;
    
    [_scrollVwImgUser setContentSize:_imgUser.frame.size];
    _scrollVwImgUser.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
   // _imgUser.center = _scrollVwImgUser.center;
    [_scrollVwImgUser addSubview:_imgUser];
    DLog(@"%f %f",_scrollVwImgUser.contentSize.width,_scrollVwImgUser.contentSize.height);
    
//    CGFloat scale;
//    CGFloat scaleToCrop;
//    if (_imgUser.frame.size.width > _imgUser.frame.size.height) {
//        //        imgFrame.size.height = _vwImageUser.frame.size.height;
//        //        imgFrame.size.width = imgFrame.size.width + _imgUser.
//        scale = _scrollVwImgUser.bounds.size.width / _imgUser.frame.size.width;
//        scaleToCrop = utilityView.cropWindow.size.height / _imgUser.frame.size.height;
//        _scrollVwImgUser.minimumZoomScale = scaleToCrop;
//        [_scrollVwImgUser setZoomScale:scale];
//    } else {
//        scale = _scrollVwImgUser.bounds.size.height / _imgUser.frame.size.height;
//        scaleToCrop = utilityView.cropWindow.size.width / _imgUser.frame.size.width;
//        _scrollVwImgUser.minimumZoomScale = scaleToCrop;
//        [_scrollVwImgUser setZoomScale:scale];
//    }

    
    //the size of the imgUser image is made equal to the scrollview by adding constraints to the content of a scrollview which would have to be zoomed was proving to be difficult in ios 7.0, but working fine in ios 8.0 and higher
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
//        NSLayoutConstraint *width =[NSLayoutConstraint
//                                    constraintWithItem:_imgUser
//                                    attribute:NSLayoutAttributeWidth
//                                    relatedBy:NSLayoutRelationEqual
//                                    toItem:_scrollVwImgUser
//                                    attribute:NSLayoutAttributeWidth
//                                    multiplier:1.1
//                                    constant:0];
//        NSLayoutConstraint *height =[NSLayoutConstraint
//                                     constraintWithItem:_imgUser
//                                     attribute:NSLayoutAttributeHeight
//                                     relatedBy:NSLayoutRelationEqual
//                                     toItem:_scrollVwImgUser
//                                     attribute:NSLayoutAttributeHeight
//                                     multiplier:1.1
//                                     constant:0];
//        [_scrollVwImgUser addConstraint:width];
//        [_scrollVwImgUser addConstraint:height];
        
//        CGRect frame = _imgUser.frame;
//        frame.size.width = _vwImageUser.frame.size.width;
//        frame.size.height = _vwImageUser.frame.size.height;
    
    
    
//        _scrollVwImgUser.zoomScale = 1.0f;
//        _imgUser.frame = CGRectMake(0, 0, _vwImageUser.frame.size.width, _vwImageUser.frame.size.height);
////        _imgUser.translatesAutoresizingMaskIntoConstraints = YES;
//        NSData *imgData = [Common getImageDataFromDefaults:keyUserImage];
//        _imgUser.image = [UIImage imageWithData:imgData]?[UIImage imageWithData:imgData]:[UIImage imageNamed:@"girl.png"];
//        CGRect imgFrame = _imgUser.frame;
//    }
    _imgUser.hidden = NO;
    
    [_vwImageUser setCornerRadius:5.0];
   // [_vwImageUser setupEmboss];
    //This is the crop view which appears when the crop buttons are clicked
    utilityView = [[UtilityView alloc] initWithFrame:CGRectMake(0, 0, _vwImageUser.frame.size.width, _vwImageUser.frame.size.height)];
    utilityView.alpha = 0.4;
    utilityView.backgroundColor = [[UIColor alloc] initWithRed:0.5 green:0.5 blue:1.0 alpha:0.0];
    utilityView.responderBelow = self.view;
    [self.vwImageUser addSubview:utilityView];
    [self.vwImageUser bringSubviewToFront:utilityView];
    
    
//    CGFloat scale;
//    CGFloat scaleToCrop;
//    if (_imgUser.image.size.width > _imgUser.image.size.height) {
//        scale = _scrollVwImgUser.bounds.size.width / _imgUser.image.size.width;
//        scaleToCrop = utilityView.cropWindow.size.height / _imgUser.image.size.height;
//        _scrollVwImgUser.minimumZoomScale = scaleToCrop;
//        [_scrollVwImgUser setZoomScale:scale];
//    } else {
//        scale = _scrollVwImgUser.bounds.size.height / _imgUser.image.size.height;
//        scaleToCrop = utilityView.cropWindow.size.width / _imgUser.image.size.width;
//        _scrollVwImgUser.minimumZoomScale = scaleToCrop;
//        [_scrollVwImgUser setZoomScale:scale];
//    }
    
    //These are the images for the animation which shows the pinch zoom indications on the given image
    UIImageView* imgFlash1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"zoom-black.png"]];
    imgFlash1.frame = _scrollVwImgUser.frame;
    imgFlash1.center = CGPointMake(_scrollVwImgUser.center.x, _scrollVwImgUser.center.y - 20);
    imgFlash1.alpha = 0.0;
    imgFlash1.contentMode = UIViewContentModeScaleAspectFit;
    [self.vwImageUser addSubview:imgFlash1];
    
    UIImageView* imgFlash2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"zoom-white.png"]];
    imgFlash2.frame = _scrollVwImgUser.frame;
    imgFlash2.center = CGPointMake(_scrollVwImgUser.center.x, _scrollVwImgUser.center.y - 20);
    imgFlash2.alpha = 0.0;
    imgFlash2.contentMode = UIViewContentModeScaleAspectFit;
    [self.vwImageUser addSubview:imgFlash2];
    
    [UIView animateWithDuration:0.7
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         imgFlash1.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.7
                                               delay: 0.0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              imgFlash1.alpha = 0.0;
                                          }
                                          completion:nil];
                     }];
    [UIView animateWithDuration:0.7
                          delay: 0.7
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         imgFlash2.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.7
                                               delay: 0.0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              imgFlash2.alpha = 0.0;
                                          }
                                          completion:nil];
                     }];
//    _scrollVwImgUser.contentSize = _imgUser.frame.size;
//    [_scrollVwImgUser setContentOffset:CGPointMake(0,0) animated:YES];
    //[_scrollVwImgUser setZoomScale:1];
    _scrollVwImgUser.minimumZoomScale = 1.0f;
    _scrollVwImgUser.maximumZoomScale = 6.0f;
//    CGFloat scale = _scrollVwImgUser.zoomScale;
//    CGRect scrollframe = _scrollVwImgUser.frame;
//    CGPoint scrollContent = _scrollVwImgUser.contentOffset;
//    CGSize scrolSize = _scrollVwImgUser.contentSize;
    //imgFrame = _imgUser.frame;
    //[self centerScrollViewContents];
    [self btnScaleFilterSelectAction:_btnScale1];
}

- (void)centerScrollViewContents {
    CGSize boundsSize = self.scrollVwImgUser.bounds.size;
    CGRect contentsFrame = self.imgUser.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.imgUser.frame = contentsFrame;
}

#pragma mark
#pragma mark Scrollview

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so you need to re-center the contents
    //[self centerScrollViewContents];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return _imgUser;
}

//when the scale buttons are clicked
//here utility view class is used, which is made for creating the crop window which is appearing on the screen when the scale buttons are clicked
- (IBAction)btnScaleFilterSelectAction:(UIButton *)sender {
    
    [self resetBtnImages:sender];
    utilityView.myArea = _vwImageUser.frame;
    int cropFrameSize;
    int cropPaddingSize;
    NSString *strBtnImage;
    switch (sender.tag) {
        case 1:
        {
            strBtnImage = @"1square";
            _lblBeadCount.text = @"= 900";
            utilityView.cropWindow = CGRectMake(0, 0, _vwImageUser.frame.size.width, _vwImageUser.frame.size.height);
            utilityView.x =self.session.xBoards = 1;
            utilityView.y =self.session.yBoards = 1;

           
        }
            break;
        case 2:
        {
            strBtnImage = @"4square";
            _lblBeadCount.text = @"= 3600";
            utilityView.cropWindow = CGRectMake(0, 0, _vwImageUser.frame.size.width, _vwImageUser.frame.size.height);
            utilityView.x =self.session.xBoards = 2;
            utilityView.y =self.session.yBoards = 2;
        }
            break;

        case 3:
        {
            strBtnImage = @"6square";
            _lblBeadCount.text = @"= 5400";
            cropFrameSize = _vwImageUser.frame.size.width/1.5;
            int cropPaddingSize = (_vwImageUser.frame.size.width - cropFrameSize)/2;
            utilityView.cropWindow = CGRectMake(cropPaddingSize, 0, cropFrameSize, _vwImageUser.frame.size.height);
            self.session.xBoards = 2;
            self.session.yBoards = 3;
            utilityView.x = 2;
            utilityView.y = 3;
        }
            break;
        case 4:
        {
            strBtnImage = @"7square";
            _lblBeadCount.text = @"= 5400";
            cropFrameSize = _vwImageUser.frame.size.height/1.5;
            int cropPaddingSize = (_vwImageUser.frame.size.height - cropFrameSize)/2;
            utilityView.cropWindow = CGRectMake(0, cropPaddingSize, _vwImageUser.frame.size.width,cropFrameSize);
            utilityView.x =self.session.xBoards = 3;
            utilityView.y =self.session.yBoards = 2;
            
        }
            break;

        default:
            break;
    }
    [sender setBackgroundImage:[UIImage imageNamed:strBtnImage] forState:UIControlStateNormal];
    [utilityView setNeedsDisplay];
}

//reset all the button images to normal
-(void)resetBtnImages:(UIButton *)sender {
  
    [_btnScale1 setBackgroundImage:[UIImage imageNamed:@"1square"] forState:UIControlStateNormal];
    [_btnScale2 setBackgroundImage:[UIImage imageNamed:@"4square"] forState:UIControlStateNormal];
    [_btnScale3 setBackgroundImage:[UIImage imageNamed:@"6square"] forState:UIControlStateNormal];
    [_btnScale4 setBackgroundImage:[UIImage imageNamed:@"7square"] forState:UIControlStateNormal];

    
}
- (IBAction)btnNextStepAction:(id)sender {
    [self cropButtonPressed];
}


//the method which is used for cropping
-(IBAction)cropButtonPressed {
    
    UIImage *img = _imgUser.image;
    UIImage *newImage =[img imageWithImage:_imgUser.image scaledToMaxWidth:_scrollVwImgUser.frame.size.width maxHeight:_scrollVwImgUser.frame.size.height];
    CGFloat scale = _scrollVwImgUser.zoomScale;
    CGRect cropSelection = CGRectMake((_scrollVwImgUser.contentOffset.x + utilityView.cropWindow.origin.x) /scale,
                                      (_scrollVwImgUser.contentOffset.y + utilityView.cropWindow.origin.y) /scale,
                                      utilityView.cropWindow.size.width /scale,
                                      utilityView.cropWindow.size.height /scale);
    
    
    //CGSize testSize = session.image.size
    ;
    CGFloat angle;
    CGRect rotatedCropSelection;
    //CGAffineTransform transform;
    //CGAffineTransform invertedTransform;
    
    CGFloat cropScale = 1.0;
    
    CGRect thumbRect = CGRectMake(0,0, utilityView.cropWindow.size.width * cropScale, utilityView.cropWindow.size.height * cropScale);
    CGRect rotatedThumbRect;
    
    switch (newImage.imageOrientation) {
        case UIImageOrientationUp:
            angle = 0;
            rotatedCropSelection = cropSelection;
            rotatedThumbRect = thumbRect;
            //transform = nil;
            break;
        case UIImageOrientationDown:
            angle = M_PI;
            rotatedCropSelection = CGRectMake(_imgUser.image.size.width - cropSelection.origin.x - cropSelection.size.width,
                                              _imgUser.image.size.height - cropSelection.origin.y - cropSelection.size.height,
                                              cropSelection.size.width,
                                              cropSelection.size.height);
            rotatedThumbRect = thumbRect;
            //transform = CGAffineTransformMakeRotation(M_PI);
            break;
        case UIImageOrientationLeft:
            angle = M_PI/2.0;
            rotatedCropSelection = CGRectMake(_imgUser.image.size.height - cropSelection.origin.y - cropSelection.size.height,
                                              cropSelection.origin.x,
                                              // session.image.size.height - cropSelection.origin.x - cropSelection.size.width,
                                              cropSelection.size.height,
                                              cropSelection.size.width);
            rotatedThumbRect = CGRectMake(0, 0, thumbRect.size.height, thumbRect.size.width);
            //transform = CGAffineTransformMakeRotation(M_PI/-2.0);
            break;
        case UIImageOrientationRight:
            angle = M_PI/-2.0;
            rotatedCropSelection = CGRectMake(cropSelection.origin.y,
                                              _imgUser.image.size.width - cropSelection.origin.x - cropSelection.size.width,
                                              // session.image.size.height - cropSelection.origin.x - cropSelection.size.width,
                                              cropSelection.size.height,
                                              cropSelection.size.width);
            rotatedThumbRect = CGRectMake(0, 0, thumbRect.size.height, thumbRect.size.width);
            
            //transform = CGAffineTransformMakeRotation(M_PI/2.0);
            break;
            
        default:
            break;
            
    }
    
    //transform = CGAffineTransformMakeRotation(angle);
    //invertedTransform = CGAffineTransformInvert(transform);
    //CGRect rotatedCropRect = CGRectApplyAffineTransform(cropSelection, invertedTransform);
//    [newImage crop2:cropSelection];
//    [newImage cropImage:newImage :cropSelection];
//
//        CGAffineTransform rectTransform;
//        rectTransform = CGAffineTransformIdentity;
//        CGImageRef imageRef = CGImageCreateWithImageInRect([newImage CGImage], CGRectApplyAffineTransform(cropSelection, rectTransform));
//    UIImage *result = [UIImage imageWithCGImage:imageRef scale:newImage.scale orientation:newImage.imageOrientation];
//    CGImageRelease(imageRef);
//    
//    imageRef = CGImageCreateWithImageInRect([newImage CGImage], rotatedCropSelection);
    
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([newImage CGImage], rotatedCropSelection);
    
    CGImageAlphaInfo	alphaInfo = CGImageGetAlphaInfo(imageRef);
    //UIImage* imageCropped = [[UIImage alloc] initWithCGImage:imageRef];
    
    
    
    //this is the working solution
    UIImage *imageCropped2 = [self crop:newImage andRect:cropSelection andScale:scale];
    
    // There's a wierdness with kCGImageAlphaNone and CGBitmapContextCreate
    // see Supported Pixel Formats in the Quartz 2D Programming Guide
    // Creating a Bitmap Graphics Context section
    // only RGB 8 bit images with alpha of kCGImageAlphaNoneSkipFirst, kCGImageAlphaNoneSkipLast, kCGImageAlphaPremultipliedFirst,
    // and kCGImageAlphaPremultipliedLast, with a few other oddball image kinds are supported
    // The images on input here are likely to be png or jpeg files
    if (alphaInfo == kCGImageAlphaNone)
        alphaInfo = kCGImageAlphaNoneSkipLast;
    
    
    // Build a bitmap context that's the size of the thumbRect
    CGContextRef bitmap = CGBitmapContextCreate(
                                                NULL,
                                                thumbRect.size.width,		// width
                                                thumbRect.size.height,		// height
                                                CGImageGetBitsPerComponent(imageRef),	// really needs to always be 8
                                                4 * thumbRect.size.width,	// rowbytes
                                                CGImageGetColorSpace(imageRef),
                                                alphaInfo
                                                );
    CGContextTranslateCTM(bitmap, thumbRect.size.width/2, thumbRect.size.height/2);
    CGContextRotateCTM(bitmap, angle);
    CGContextTranslateCTM(bitmap, rotatedThumbRect.size.width/-2, rotatedThumbRect.size.height/-2);
    
    
    // Draw into the context, this scales the image
    CGContextDrawImage(bitmap, CGRectMake(0,0,rotatedThumbRect.size.width, rotatedThumbRect.size.height), imageRef);
    
    // Get an image from the context and a UIImage
    CGImageRef	ref = CGBitmapContextCreateImage(bitmap);
    
    //CGContextRelease(bitmap);	// ok if NULL
    //CGImageRelease(ref);
    
    
    
    
    
    
    //UIImage* temp1 = [UIImage imageWithCGImage:imageRef scale:scale orientation:UIImageOrientationUp];
    
    //this is also working solution, and this is being used
    UIImage *temp = [self cropImage:cropSelection andScale:scale];
    //NSString *strImage = getImageString(temp);
    NSData *dataImage = [NSData dataWithData:UIImagePNGRepresentation(temp)];
    [Common setImageDataToDefaults:keyScaledImage value:dataImage];
    
    
    [Common setUserDefaults:keyxBoards value:[NSString stringWithFormat:@"%d",self.session.xBoards]];
    [Common setUserDefaults:keyyBoards value:[NSString stringWithFormat:@"%d",self.session.yBoards]];
    CGImageRelease(imageRef);
    
    PearlifyViewController *pearlify = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PearlifyViewController class])];
    pearlify.session = self.session;
    [self.navigationController pushViewController:pearlify animated:YES];

    
    //UIImage *imgTemp = [self croppedImageWithImage:newImage zoom:scale];
//    _imgUser.image = temp;
//    _imgUser.contentMode = UIViewContentModeScaleAspectFit;
    //[UIImage imageWithCGImage:ref scale:1.0f orientation:rotation];
    //session.scaledImage = resizedImage(temp, CGRectMake(0, 0, utilityView.cropWindow.size.width, utilityView.cropWindow.size.height));
    
    
    //	[temp release];
//    PearlifyController* nextController = [[PearlifyController alloc] initWithNibName:@"PearlifyController" bundle:nil];
//    nextController.session = self.session;
//    /*else {
//     [nextController release];
//     nextController = nil;
//     nextController = [[PearlifyController alloc] initWithNibName:@"PearlifyController" bundle:nil];
//     nextController.session = self.session;
//     }*/
//    
//    [self.navigationController pushViewController:nextController animated:YES];
    //[nextController viewDidLoad];
}

-(UIImage *)crop:(UIImage *)image andRect:(CGRect)rect andScale:(CGFloat)scale{
    UIGraphicsBeginImageContextWithOptions(rect.size, false, scale);
     [image drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];
    UIImage *cropped_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cropped_image;
}

- (UIImage *)cropImage:(CGRect)rect andScale:(CGFloat)scale{
    //[self.imgUser.layer drawInContext:UIGraphicsGetCurrentContext()];
    
//    UIImageView *tempImg = [[UIImageView alloc]initWithImage:_imgUser.image];
    UIGraphicsBeginImageContext(self.imgUser.frame.size);
//    UIGraphicsBeginImageContextWithOptions(tempImg.bounds.size, tempImg.opaque, 0.0);
    [_imgUser.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *fullScreenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGImageRef croppedImage = CGImageCreateWithImageInRect(fullScreenshot.CGImage, rect);
    UIImage *crop = [UIImage imageWithCGImage:croppedImage];// resizedImageForProperOrientation:_imgUser.frame.size interpolationQuality:1.0];
    //crop = [crop resizedImageForProperOrientation:_imgUser.frame.size interpolationQuality:1.0];
    CGImageRelease(croppedImage);
    return crop;
}

- (UIImage *)croppedImageWithImage:(UIImage *)image zoom:(CGFloat)zoom {
    CGFloat zoomReciprocal = 1.0f / zoom;
    CGFloat xOffset = image.size.width / _scrollVwImgUser.contentSize.width;
    CGFloat yOffset = image.size.height / _scrollVwImgUser.contentSize.height;
    
    CGRect croppedRect = CGRectMake(self.scrollVwImgUser.contentOffset.x * xOffset,
                                    self.scrollVwImgUser.contentOffset.y * yOffset,
                                    image.size.width * zoomReciprocal,
                                    image.size.height * zoomReciprocal);
    
    CGImageRef croppedImageRef = CGImageCreateWithImageInRect([image CGImage], croppedRect);
    UIImage *croppedImage = [[UIImage alloc] initWithCGImage:croppedImageRef scale:[image scale] orientation:[image imageOrientation]];
    CGImageRelease(croppedImageRef);
    return croppedImage;
    
}



@end
